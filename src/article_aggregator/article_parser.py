import codecs
import os
import tempfile
from random import randint
from subprocess import call, DEVNULL
from xml.etree import ElementTree

import requests
from newspaper import Article
from requests.exceptions import HTTPError


def sanitize_url(url):
    from urllib.parse import urlparse, urlunparse, parse_qs, urlencode

    url_remove_queries = [
        "utm_source",
        "utm_medium",
        "utm_term",
        "utm_content",
        "utm_campaign",
        "ref"
    ]

    u = urlparse(url)
    query = parse_qs(u.query)

    for q in url_remove_queries:
        query.pop(q, None)

    u = u._replace(query=urlencode(query, True))
    u = u._replace(fragment=None)

    return urlunparse(u)


def clean_emoji(text):
    import re

    try:
        # Wide UCS-4 build
        myre = re.compile(u'['
                          u'\U0001F300-\U0001F64F'
                          u'\U0001F680-\U0001F6FF'
                          u'\u2600-\u26FF\u2700-\u27BF]+',
                          re.UNICODE)
    except re.error:
        # Narrow UCS-2 build
        myre = re.compile(u'('
                          u'\ud83c[\udf00-\udfff]|'
                          u'\ud83d[\udc00-\ude4f\ude80-\udeff]|'
                          u'[\u2600-\u26FF\u2700-\u27BF])+',
                          re.UNICODE)

    return myre.sub('', text)


class ArticleParser(object):
    request_headers = {'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}

    def get_contents(self):
        response = requests.get(self.url, timeout=7, headers=self.request_headers)
        response.raise_for_status()
        self.canonical = response.url

        contents = response.content.decode('UTF-8', errors='ignore')
        return contents

    def get_contents_alt(self):

        save_path = '{}.html'.format(os.path.join(tempfile.gettempdir(), str(randint(1, 90000))))
        ssl_exec_list = ['wget', '-O', save_path, '-t', '7', self.url]
        call(ssl_exec_list, stdout=DEVNULL, stderr=DEVNULL)

        contents = ''

        if os.path.isfile(save_path):
            with codecs.open(save_path) as f:
                contents = f.read()
                f.close()

            os.remove(save_path)

        return contents

    def sanitize_html(self):
        self.html = clean_emoji(self.html)

    def __init__(self, url):
        self.url = url

        self.parsed_title = None
        self.parsed_summary = None
        self.parsed_image = None
        self.canonical = None
        self.publish_date = None

    def parse(self):
        try:
            self.html = self.get_contents()
        except HTTPError as ex:
            if '403' in str(ex):
                self.html = self.get_contents_alt()

        self.sanitize_html()

        article = Article(self.url)
        article.download(self.html)
        article.parse()

        # detect is there a form tag in article
        article_has_form = False
        if article.clean_top_node is not None:
            article_html = ElementTree.tostring(article.clean_top_node).decode('utf-8', 'ignore')
            article_has_form = '<form' in article_html

        self.parsed_title = article.title.strip()
        self.parsed_image = article.meta_img.strip()
        if self.parsed_image == '':
            self.parsed_image = None

        # prevent date extraction from url
        self.publish_date = article.extractor.get_publishing_date('', article.clean_doc)

        # i prefer twitter description if it exists
        meta_data = article.meta_data
        try:
            description = meta_data['twitter']['description']
        except KeyError:
            description = article.meta_description

        summary = ''

        if article_has_form:
            # if article containts a form use description as summary
            summary = description
        else:
            # fetch summary from text
            text_parts = article.text.split('\n')
            p = 0
            for part in text_parts:
                if len(part) > 50:
                    summary = '{}\n{}'.format(summary, part.strip('\n'))
                    p += 1
                if p == 2:
                    break

            # if summary is small, use description
            if len(summary) < 150:
                summary = description

        self.summary = summary.strip('\n')

        # fetch canonical link
        if article.canonical_link:
            self.canonical = article.canonical_link

        if '?' in self.canonical:
            try:
                self.canonical = sanitize_url(self.canonical)
            except Exception:
                pass
