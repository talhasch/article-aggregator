import logging
from datetime import datetime

import pytz
from dateutil.parser import parse as dateparse
from flask import Flask
from flask_admin import Admin
from flask_assets import Environment, Bundle
from flask_sqlalchemy import SQLAlchemy

from article_aggregator.config import config_get, config_get_int, config_get_bool
from article_aggregator.models import DataSource, Account, App, Project, AutoPostAccount
from article_aggregator.ui.views import DataSourceView, IndexView, AccountView, AppView, ProjectView, \
    AutoPostAccountView

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
db = None


def __flask_setup():
    global app
    app.config['CSRF_ENABLED'] = True
    app.config['DEVELOPMENT'] = config_get('FLASK', 'DEVELOPMENT')
    app.config['DEBUG'] = config_get('FLASK', 'DEBUG')
    app.config['SECRET_KEY'] = config_get('FLASK', 'SECRET_KEY')
    app.config['SQLALCHEMY_DATABASE_URI'] = config_get('SQLALCHEMY', 'DATABASE_URI')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config_get_bool('SQLALCHEMY', 'TRACK_MODIFICATIONS')
    app.config['SQLALCHEMY_POOL_SIZE'] = config_get_int('SQLALCHEMY', 'POOL_SIZE')


def __db_setup():
    global app, db

    db = SQLAlchemy(app)


def __template_setup():
    global app

    def date_format(date_str):
        if not type(date_str) == datetime:
            the_date = dateparse(date_str)
        else:
            the_date = date_str

        the_date = the_date.astimezone(pytz.UTC)

        return the_date.strftime('%Y-%m-%d %H:%M')

    app.jinja_env.globals['date_format'] = date_format


def __asset_setup():
    global app, db

    assets = Environment(app)

    assets.register('data-source-form', Bundle('plugin/typeahead/typeahead.bundle.min.js', 'js/data-source-form.js',
                                               output='gen/data-source-form.js'))

    assets.register('account-form', Bundle('js/account-form.js',
                                           output='gen/account-form.js'))

    assets.register('project-form', Bundle('plugin/typeahead/typeahead.bundle.min.js', 'js/project-form.js',
                                           output='gen/project-form.js'))

    assets.register('auto-post-account-fb-form', Bundle('js/auto-post-account-fb-form.js',
                                                        output='gen/auto-post-account-fb-form.js'))

    assets.register('auto-post-account-tw-form', Bundle('js/auto-post-account-tw-form.js',
                                                        output='gen/auto-post-account-tw-form.js'))

    css_bundle1 = Bundle('css/custom.scss', filters='pyscss', output='gen/custom.css')
    assets.register('custom-css', css_bundle1)


def __admin_setup():
    global app, db

    admin = Admin(app, name='Aggregator', template_mode='bootstrap3', url='/',
                  index_view=IndexView(url='/', endpoint='admin', name='Home'))
    admin.add_view(DataSourceView(DataSource, db.session, name='Data Sources'))
    admin.add_view(AccountView(Account, db.session, name='Users', category='Credentials'))
    admin.add_view(AppView(App, db.session, name='Apps', category='Credentials'))
    admin.add_view(ProjectView(Project, db.session, name='Projects'))
    admin.add_view(AutoPostAccountView(AutoPostAccount, db.session, name='Auto Post'))


def __api_setup():
    global app

    from article_aggregator.ui.api import api_bp

    app.register_blueprint(api_bp)


def __run_server():
    global app

    server_address = config_get('SERVER', 'ADDRESS')
    server_port = config_get_int('SERVER', 'PORT')

    app.run(host=server_address, port=server_port)

__flask_setup()
__db_setup()
__template_setup()
__asset_setup()
__admin_setup()
__api_setup()


def main():
    __run_server()


if __name__ == '__main__':
    main()
