import json
import re
from datetime import datetime
from math import ceil

import pytz
import tldextract
import tweepy
from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.index.helper import make_search_body
from article_aggregator.models import DataSource, App, Keyword, ProjectResult, Project, Result, AutoPostAccount
from article_aggregator.tasks.tasks import scrape_rss, scrape_twitter, scrape_facebook
from elasticsearch import Elasticsearch
from facebook import GraphAPI, GraphAPIError
from flask import Markup
from flask import request, url_for, abort, redirect, session
from flask_admin import expose, BaseView
from flask_admin.contrib.sqla import ModelView
from flask_admin.form.rules import Field
from flask_admin.helpers import flash
from flask_admin.model import typefmt
from sqlalchemy import and_
from sqlalchemy import create_engine
from sqlalchemy import func
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import sessionmaker
from wtforms import Form, StringField, SelectField
from wtforms import validators
from wtforms.validators import DataRequired

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')
db_engine = create_engine(db_uri)

es_host = config_get('ES', 'HOST')
index_name = config_get('ES', 'INDEX_NAME')
index_type = config_get('ES', 'TYPE_NAME')
es = Elasticsearch(es_host)


def page_generator(o):
    args = request.args.copy()
    args['pa'] = o
    return url_for(request.endpoint, **args)


def alphanumeric_validate(form, field):
    if not re.match(r'^[a-z0-9-]+$', field.data):
        raise validators.ValidationError('Only lower alphanumeric and dash')


def data_source_groups():
    global db_engine
    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()
    distinct_data = db_session.query(DataSource.group).distinct()

    items = []
    for item in distinct_data:
        items.append(item[0])

    db_session.close()
    return items


def data_source_categories():
    global db_engine
    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()
    distinct_data = db_session.query(DataSource.category).distinct()

    items = []
    for item in distinct_data:
        items.append(item[0])

    db_session.close()
    return items


class CustomizableField(Field):
    def __init__(self, field_name, render_field='lib.render_field', field_args={}):
        super(CustomizableField, self).__init__(field_name, render_field)
        self.extra_field_args = field_args

    def __call__(self, form, form_opts=None, field_args={}):
        field_args.update(self.extra_field_args)
        return super(CustomizableField, self).__call__(form, form_opts, field_args)


class DataSourceView(ModelView):
    @expose('/groups')
    def groups(self):
        items = data_source_groups()
        return json.dumps(items)

    @expose('/categories')
    def categories(self):
        items = data_source_categories()
        return json.dumps(items)

    @expose('/rss', methods=('GET', 'POST'))
    def rss(self):
        address = request.form['address']
        if not (address.startswith('http://') or address.startswith('https://')):
            return json.dumps({'status': 0})

        try:
            the_data = scrape_rss(address)
        except Exception as ex:
            return json.dumps({'status': 2, 'msg': str(ex)})

        domain = None
        if the_data['website'] is not None:
            try:
                tld = tldextract.extract(the_data['website'])
                domain = tld.domain
            except Exception as ex:
                pass

        the_data['domain'] = domain

        return json.dumps({'status': 1, 'data': the_data}, default=lambda x: str(x))

    @expose('/twitter', methods=('GET', 'POST'))
    def twitter(self):
        address = request.form['address']

        try:
            the_data = scrape_twitter(address)
        except Exception as ex:
            return json.dumps({'status': 2, 'msg': str(ex)})

        domain = None
        if the_data['website'] is not None:
            try:
                tld = tldextract.extract(the_data['website'])
                domain = tld.domain
            except Exception as ex:
                pass

        the_data['domain'] = domain

        return json.dumps({'status': 1, 'data': the_data}, default=lambda x: str(x))

    @expose('/facebook', methods=('GET', 'POST'))
    def facebook(self):
        address = request.form['address']

        try:
            the_data = scrape_facebook(address)

        except Exception as ex:
            return json.dumps({'status': 2, 'msg': str(ex)})

        domain = None
        if the_data['website'] is not None:
            try:
                tld = tldextract.extract(the_data['website'])
                domain = tld.domain
            except Exception as ex:
                pass

        the_data['domain'] = domain

        return json.dumps({'status': 1, 'data': the_data}, default=lambda x: str(x))

    @expose('/reset')
    def reset(self):
        source_id = request.args.get('id', type=int)
        if not source_id:
            abort(400)

        global db_engine

        try:
            delete = Result.__table__.delete().where(
                Result.source_id == source_id).where(Result.status.in_((RESULT_STATUS_PARSE_FAILED,
                                                                        RESULT_STATUS_ERR_DUPLICATE,
                                                                        RESULT_STATUS_ERR_NOT_ARTICLE,
                                                                        RESULT_STATUS_ERR_WRONG_DOMAIN)))

            db_engine.execute(delete)
        except SQLAlchemyError as ex:
            pass

        flash('Data source reset successful')
        return redirect(url_for('datasource.edit_view', id=source_id))

    list_template = 'data_source/list.jinja2'
    create_template = 'data_source/create.jinja2'
    edit_template = 'data_source/edit.jinja2'
    column_list = ['id', 'category', 'group', 'title', 'address', 'last_run', 'type', 'status']
    column_filters = ['title', 'group', 'category', 'address']
    column_choices = {
        'type': [
            (DATASOURCE_TYPE_RSS, 'Rss Feed'),
            (DATASOURCE_TYPE_TWITTER, 'Twitter Account'),
            (DATASOURCE_TYPE_FACEBOOK, 'Facebook Page')
        ],
        'status': [
            (DATASOURCE_STATUS_ON, 'On'),
            (DATASOURCE_STATUS_OFF, 'Off')
        ]
    }

    form_columns = ['type', 'address', 'title', 'filters', 'category', 'group', 'status']
    form_choices = {
        'type': [
            (str(DATASOURCE_TYPE_RSS), 'Rss Feed'),
            (str(DATASOURCE_TYPE_TWITTER), 'Twitter Account'),
            (str(DATASOURCE_TYPE_FACEBOOK), 'Facebook Page')
        ],
        'status': [
            (str(DATASOURCE_STATUS_ON), 'On'),
            (str(DATASOURCE_STATUS_OFF), 'Off')
        ]
    }

    form_args = {
        'address': {
            'label': 'Address',
            'description': 'For Rss Feed enter full url. For Twitter and Facebook enter username of account.'
        },
        'title': {
            'label': 'Title',
            'description': 'Title of data source.'
        },
        'filters': {
            'label': 'Domain Filters',
            'description': 'Enter domain name prefixes for avoid unwanted results from different websites. ' \
                           'Use comma for multiple values e.g: bbc'
        },
        'category': {
            'label': 'Category',
            'description': 'Only lower alphanumeric and dash. e.g: manchester-united',
            'validators': [alphanumeric_validate]
        },
        'group': {
            'label': 'Group',
            'description': 'Only lower alphanumeric and dash. e.g: bbc',
            'validators': [alphanumeric_validate]
        }
    }

    def date_format(view, value):
        if value is None:
            return ''

        return value.astimezone(pytz.UTC).strftime('%Y-%m-%d %H:%M:%S')

    MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
    MY_DEFAULT_FORMATTERS.update({
        datetime: date_format
    })

    column_type_formatters = MY_DEFAULT_FORMATTERS

    def after_model_change(self, form, model, is_created):
        if not is_created:
            global db_engine

            # Update data source titles in ProjectResult
            new_title = form.title.data

            try:
                update_args = {
                    ProjectResult.result_source_title.name: new_title
                }

                update = ProjectResult.__table__.update().values(**update_args).where(
                    ProjectResult.result_source_id == model.id)
                db_engine.execute(update)
            except SQLAlchemyError as ex:
                pass

            # Trigger re-index data source's results
            try:
                update_args = {
                    Result.status.name: RESULT_STATUS_REINDEX
                }
                update = Result.__table__.update().values(**update_args).where(
                    Result.source_id == model.id).where(Result.status == RESULT_STATUS_INDEXED)
                db_engine.execute(update)
            except SQLAlchemyError as ex:
                pass

    def on_model_change(self, form, model, is_created):

        ds_type = form.type.data
        address = form.address.data

        if ds_type == str(DATASOURCE_TYPE_RSS):
            try:
                scrape_rss(address)
            except Exception as ex:
                raise validators.ValidationError(str(ex))

        if ds_type == str(DATASOURCE_TYPE_TWITTER):
            try:
                scrape_twitter(address)
            except Exception as ex:
                raise validators.ValidationError(str(ex))

        if ds_type == str(DATASOURCE_TYPE_FACEBOOK):
            try:
                scrape_facebook(address)
            except Exception as ex:
                raise validators.ValidationError(str(ex))

        super().on_model_change(form, model, is_created)


class AppView(ModelView):
    list_template = 'app/list.jinja2'
    create_template = 'app/create.jinja2'
    edit_template = 'app/edit.jinja2'

    form_columns = ['name', 'type', 'key', 'secret', 'status']
    column_list = ['name', 'type', 'status']
    column_choices = {
        'type': [
            (APP_TYPE_TWITTER, 'Twitter'),
            (APP_TYPE_FACEBOOK, 'Facebook')
        ],
        'status': [
            (APP_STATUS_ON, 'On'),
            (APP_STATUS_OFF, 'Off')
        ]
    }

    form_choices = {
        'type': [
            (str(APP_TYPE_TWITTER), 'Twitter'),
            (str(APP_TYPE_FACEBOOK), 'Facebook')
        ],
        'status': [
            (str(APP_STATUS_ON), 'On'),
            (str(APP_STATUS_OFF), 'Off')
        ]
    }


class AccountView(ModelView):
    @expose('/get_token')
    def get_token(self):
        app_id = request.args.get('app', type=int)
        if not app_id:
            abort(400)

        global db_engine
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()
        app = db_session.query(App).filter(App.id == app_id).first()
        db_session.close()

        if app is None:
            abort(400)

        if app.type == APP_TYPE_TWITTER:
            return redirect(url_for('account.get_token_tw', app=app_id))

        if app.type == APP_TYPE_FACEBOOK:
            return redirect(url_for('account.get_token_fb', app=app_id))

    @expose('/get_token_tw')
    def get_token_tw(self):
        app_id = request.args.get('app', type=int)
        if not app_id:
            if 'app' in session:
                app_id = session['app']

        if not app_id:
            abort(400)

        global db_engine
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()
        app = db_session.query(App).filter(App.id == app_id).first()
        db_session.close()

        if app is None:
            abort(400)

        oauth_token = request.args.get("oauth_token", default=None)
        oauth_verifier = request.args.get("oauth_verifier", default=None)

        callback = config_get('TW', 'CALLBACK')

        if oauth_token is not None and oauth_verifier is not None and 'request_token' in session:
            auth = tweepy.OAuthHandler(app.key, app.secret, callback=callback)
            token = session['request_token']
            del session['request_token'], session['app']

            auth.request_token = token
            auth.get_access_token(oauth_verifier)
            api = tweepy.API(auth)
            me = api.me()
            screen_name = me.screen_name
            access_token = auth.access_token
            access_token_secret = auth.access_token_secret
            return '''
            <script>opener.callback('{}','{}','{}'); window.close(this);</script>
            '''.format(screen_name, access_token, access_token_secret)

        session['app'] = app_id
        auth = tweepy.OAuthHandler(app.key, app.secret, callback=callback)
        redirect_url = auth.get_authorization_url()
        session['request_token'] = auth.request_token

        return redirect(redirect_url)

    @expose('/get_token_fb')
    def get_token_fb(self):
        app_id = request.args.get('app', type=int)
        if not app_id:
            if 'app' in session:
                app_id = session['app']

        if not app_id:
            abort(400)

        global db_engine
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()
        app = db_session.query(App).filter(App.id == app_id).first()
        db_session.close()

        if app is None:
            abort(400)

        code = request.args.get('code', default=None)
        error = request.args.get('error', default=None)
        error_description = request.args.get('error_description', default='')
        callback = config_get('FB', 'CALLBACK')

        if error:
            del session['app']

            return 'Error: {}'.format(error_description)

        if code:
            del session['app']

            graph = GraphAPI()
            try:
                access_token = graph.get_access_token_from_code(code, callback, app.key, app.secret)
                user_access_token = access_token['access_token']
            except (GraphAPIError, KeyError) as ex:
                return 'Error: Could not get access token'

            try:
                graph = GraphAPI(user_access_token)
                me = graph.get_object('/me')
                screen_name = me['name']
            except (GraphAPIError, KeyError) as ex:
                return 'Error: Could not get user data'

            return '''
            <script>opener.callback('{}','{}','{}'); window.close(this);</script>
            '''.format(screen_name, user_access_token, '')

        session['app'] = app_id
        authorize_url = "https://www.facebook.com/dialog/oauth?client_id=%s&redirect_uri=%s&scope=%s" % (
            app.key, callback, ','.join(['email']))

        return redirect(authorize_url)

    list_template = 'account/list.jinja2'
    create_template = 'account/create.jinja2'
    edit_template = 'account/edit.jinja2'

    form_columns = ['app', 'name', 'token', 'token_secret', 'status']
    column_list = ['app', 'name', 'status']

    column_choices = {
        'status': [
            (ACCOUNT_STATUS_ON, 'On'),
            (ACCOUNT_STATUS_OFF, 'Off')
        ]
    }

    form_choices = {
        'status': [
            (str(ACCOUNT_STATUS_ON), 'On'),
            (str(ACCOUNT_STATUS_OFF), 'Off')
        ]
    }


class IndexView(BaseView):
    @expose('/')
    def index(self):
        global es

        inline = request.args.get('inline', default=0, type=int)
        data_source_id = request.args.get('ds', default=None, type=int)
        data_source_category = request.args.get('ds_cat', default=None)
        query_str = request.args.get('query', default='')

        if query_str:
            query_str = query_str.strip()

        page_size = 20
        cur_page = request.args.get("pa", default=0, type=int)
        if cur_page == 0:
            start_from = 0
        else:
            start_from = page_size * cur_page

        search_body = make_search_body(query_str, data_source_id=data_source_id,
                                       data_source_category=data_source_category, start_from=start_from,
                                       page_size=page_size)

        try:
            search_results = es.search(index=index_name, doc_type=index_type, body=search_body)
            total = search_results['hits']['total']
            hits = search_results['hits']['hits']
            num_pages = int(ceil(total / float(page_size)))
        except Exception as ex:
            total = 0
            hits = []
            num_pages = 0

        data = []
        for hit in hits:
            row = hit['_source']

            if 'highlight' in hit:
                if 'title' in hit['highlight']:
                    row['title'] = hit['highlight']['title'][0]

                if 'summary' in hit['highlight']:
                    row['summary'] = hit['highlight']['summary'][0]

            data.append(row)

        source_categories = data_source_categories()

        if inline:
            template = 'inline'
        else:
            template = 'index'

        return self.render('index/{}.jinja2'.format(template), data=data, total_count=total, query=query_str,
                           source_categories=source_categories,
                           num_pages=num_pages, page=cur_page, page_generator=page_generator,
                           inline=inline, data_source_id=data_source_id, data_source_category=data_source_category)


class ProjectView(ModelView):
    @expose('/categories')
    def categories(self):
        items = data_source_categories()
        return json.dumps(items)

    @expose('/block')
    def block(self):
        project_result_id = request.args.get('id', type=int, default=None)
        if project_result_id is None:
            abort(404)

        global db_engine
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()
        project_result = db_session.query(ProjectResult).filter(ProjectResult.id == project_result_id).first()
        if project_result is None:
            db_session.close()
            abort(404)

        project_result.blocked = 1
        db_session.commit()
        db_session.close()

        return json.dumps({})

    @expose('/detail')
    def detail(self):
        project_id = request.args.get('id', type=int, default=None)
        if project_id is None:
            abort(404)

        global db_engine
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()
        project = db_session.query(Project).filter(Project.id == project_id).first()
        if project is None:
            db_session.close()
            abort(404)

        source = request.args.get('s', default='')

        page_size = 20
        cur_page = request.args.get("pa", default=0, type=int)
        if cur_page == 0:
            start_from = 0
        else:
            start_from = page_size * cur_page

        filters = [ProjectResult.project_id == project_id, ProjectResult.blocked == 0]
        if source:
            filters.append(ProjectResult.result_source_title == source)

        data = db_session.query(ProjectResult) \
            .filter(and_(*filters)) \
            .order_by(ProjectResult.result_published.desc()).offset(start_from).limit(page_size).all()

        total_count = db_session.query(func.count(ProjectResult.id)).select_from(ProjectResult) \
            .filter(and_(*filters)) \
            .scalar()

        sources = db_session.query(ProjectResult.result_source_title.label('title'),
                                   func.count(ProjectResult.result_source_title).label('cnt')) \
            .filter(ProjectResult.project_id == project_id) \
            .filter(ProjectResult.blocked == 0) \
            .group_by(ProjectResult.result_source_title) \
            .order_by(ProjectResult.result_source_title.label('cnt').desc()).all()

        num_pages = int(ceil(total_count / float(page_size)))

        return self.render('project/detail.jinja2', project_id=project_id, project=project, total_count=total_count,
                           data=data, sources=sources, selected_source=source,
                           num_pages=num_pages, page=cur_page, page_generator=page_generator)

    create_template = 'project/create.jinja2'
    edit_template = 'project/edit.jinja2'
    list_template = 'project/list.jinja2'

    form_columns = ['title', 'slug', 'source_category']
    column_list = ['title']
    column_filters = ['title']
    column_sortable_list = []

    def _title_formatter(view, context, model, name):
        return Markup('<a class="project-table-link" href="{}">{}</a>'.format(url_for('project.detail', id=model.id),
                                                                              model.title))

    column_formatters = {'title': _title_formatter}

    inline_models = [(Keyword, dict(
        form_columns=['id', 'query'],
        form_widget_args={
            'query': {
                'maxlength': 100
            }
        }
    ))]


def get_projects():
    global db_engine

    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()
    projects = db_session.query(Project).order_by(Project.id.desc()).all()
    project_list = [('', '--')]
    for project in projects:
        project_list.append((str(project.id), project.title))

    db_session.close()

    return project_list


class FBForm(Form):
    project = SelectField('Project', validators=[DataRequired()])
    key = StringField('Application id', validators=[DataRequired()])
    secret = StringField('Application secret', validators=[DataRequired()])
    key2 = StringField('Page Access Token', validators=[DataRequired()])
    name = StringField('Page Name', validators=[DataRequired()])
    username = StringField('Page Id', validators=[DataRequired()])
    status = SelectField('Status', choices=[('1', 'On'), ('2', 'Off')], validators=[DataRequired()])
    tags = StringField('Hashtags', [validators.Length(max=80)], description='Comma separated. Without #',
                       render_kw={'maxlength': '80'})

    def __init__(self, *args, **kwargs):
        super(FBForm, self).__init__(*args, **kwargs)
        self.project.choices = get_projects()


class TWForm(Form):
    project = SelectField('Project', validators=[DataRequired()])
    key = StringField('API key', validators=[DataRequired()])
    secret = StringField('API secret', validators=[DataRequired()])
    key2 = StringField('Access token', validators=[DataRequired()])
    secret2 = StringField('Access token secret', validators=[DataRequired()])
    name = StringField('Account Name', validators=[DataRequired()])
    username = StringField('Account Id', validators=[DataRequired()])
    status = SelectField('Status', choices=[('1', 'On'), ('2', 'Off')], validators=[DataRequired()])
    tags = StringField('Hashtags', [validators.Length(max=80)], description='Comma separated. Without #',
                       render_kw={'maxlength': '80'})

    def __init__(self, *args, **kwargs):
        super(TWForm, self).__init__(*args, **kwargs)
        self.project.choices = get_projects()


class AutoPostAccountView(ModelView):
    @expose('/get_fb_page_token')
    def get_fb_page_token(self):

        key = request.args.get('key')
        secret = request.args.get('secret')

        code = request.args.get('code', default=None)
        error = request.args.get('error', default=None)
        error_description = request.args.get('error_description', default='')
        callback = config_get('FB', 'CALLBACK_PAGE')

        if error:
            return 'Error: {}'.format(error_description)

        if code:
            key = session['fb_client_id']
            secret = session['fb_client_secret']

            graph = GraphAPI()
            try:
                access_token = graph.get_access_token_from_code(code, callback, key, secret)
                user_access_token = access_token['access_token']
            except Exception as ex:
                return 'Error: Could not get access token: {}'.format(str(ex))

            try:
                graph = GraphAPI(user_access_token)
                resp = graph.get_object('/me/accounts?type=page&limit=200')
                pages = resp['data']
                return self.render('auto_post_account/get_fb_page_token.jinja2', pages=pages)

            except Exception as ex:
                return 'Error: Could not get user data: {}'.format(str(ex))

        session['fb_client_id'] = key
        session['fb_client_secret'] = secret

        authorize_url = "https://www.facebook.com/dialog/oauth?client_id=%s&scope=%s&redirect_uri=%s" % (
            key, ','.join(['manage_pages', 'publish_pages']), callback)

        return redirect(authorize_url)

    @expose('/create_fb', methods=['GET', 'POST'])
    def create_fb(self):

        form = FBForm(request.form)

        if request.method == 'POST' and form.validate():
            project = form.project.data
            key = form.key.data
            secret = form.secret.data
            key2 = form.key2.data
            name = form.name.data
            username = form.username.data
            status = form.status.data
            tags = form.tags.data

            api_flag = False

            try:
                graph = GraphAPI(key2)
                graph.get_object('/me')
                api_flag = True
            except (GraphAPIError, KeyError) as ex:
                flash(str(ex), 'danger')

            if api_flag:
                global db_engine
                db_session_maker = sessionmaker(bind=db_engine)
                db_session = db_session_maker()

                account = AutoPostAccount()
                account.project_id = project
                account.type = AUTO_POST_ACCOUNT_TYPE_FACEBOOK
                account.key = key
                account.secret = secret
                account.key2 = key2
                account.name = name
                account.username = username
                account.status = status
                account.tags = tags

                db_session.add(account)

                try:
                    db_session.commit()
                    db_session.close()
                    flash('Record was successfully created.', 'success')
                    return redirect(url_for('autopostaccount.index_view'))
                except Exception as ex:
                    db_session.rollback()
                    db_session.close()
                    flash(str(ex), 'danger')

        return self.render('auto_post_account/create_fb.jinja2', form=form)

    @expose('/edit_fb', methods=['GET', 'POST'])
    def edit_fb(self):
        id_ = request.args.get('id', type=int, default=None)
        if not id_:
            abort(400)

        global db_engine
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()

        account = db_session.query(AutoPostAccount).filter(AutoPostAccount.id == id_).filter(
            AutoPostAccount.type == AUTO_POST_ACCOUNT_TYPE_FACEBOOK).first()
        if account is None:
            abort(404)

        form = FBForm(request.form)

        if request.method == 'GET':
            form.project.data = str(account.project_id)
            form.key.data = account.key
            form.secret.data = account.secret
            form.key2.data = account.key2
            form.name.data = account.name
            form.username.data = account.username
            form.status.data = str(account.status)
            form.tags.data = account.tags

        if request.method == 'POST' and form.validate():
            project = form.project.data
            key = form.key.data
            secret = form.secret.data
            key2 = form.key2.data
            name = form.name.data
            username = form.username.data
            status = form.status.data
            tags = form.tags.data

            api_flag = False

            try:
                graph = GraphAPI(key2)
                graph.get_object('/me')
                api_flag = True
            except (GraphAPIError, KeyError) as ex:
                flash(str(ex), 'danger')

            if api_flag:
                db_session_maker = sessionmaker(bind=db_engine)
                db_session = db_session_maker()
                account = db_session.query(AutoPostAccount).filter(AutoPostAccount.id == id_).first()

                account.project_id = project
                account.key = key
                account.secret = secret
                account.key2 = key2
                account.name = name
                account.username = username
                account.status = status
                account.tags = tags

                try:
                    db_session.commit()
                    db_session.close()
                    flash('Record was successfully saved.', 'success')
                    return redirect(url_for('autopostaccount.edit_fb', id=id_))
                except Exception as ex:
                    db_session.rollback()
                    db_session.close()
                    flash(str(ex), 'danger')

        return self.render('auto_post_account/edit_fb.jinja2', id=id_, form=form)

    @expose('/validate_tw', methods=['GET'])
    def validate_tw(self):
        key = request.args.get('key', default='')
        secret = request.args.get('secret', default='')
        key2 = request.args.get('key2', default='')
        secret2 = request.args.get('secret2', default='')

        auth = tweepy.OAuthHandler(key, secret)
        auth.access_token = key2
        auth.access_token_secret = secret2
        api = tweepy.API(auth)

        try:
            me = api.me()
            ret_val = {'status': 1, 'screen_name': me.screen_name, 'id': me.id_str}
            return json.dumps(ret_val)
        except Exception as ex:
            ret_val = {'status': 0, 'message': str(ex)}
            return json.dumps(ret_val)

    @expose('/create_tw', methods=['GET', 'POST'])
    def create_tw(self):

        form = TWForm(request.form)

        if request.method == 'POST' and form.validate():
            project = form.project.data
            key = form.key.data
            secret = form.secret.data
            key2 = form.key2.data
            secret2 = form.secret2.data
            name = form.name.data
            username = form.username.data
            status = form.status.data
            tags = form.tags.data

            api_flag = False

            auth = tweepy.OAuthHandler(key, secret)
            auth.access_token = key2
            auth.access_token_secret = secret2
            api = tweepy.API(auth)

            try:
                api.me()
                api_flag = True
            except Exception as ex:
                flash(str(ex), 'danger')

            if api_flag:
                global db_engine

                db_session_maker = sessionmaker(bind=db_engine)
                db_session = db_session_maker()

                account = AutoPostAccount()
                account.project_id = project
                account.type = AUTO_POST_ACCOUNT_TYPE_TWITTER
                account.key = key
                account.secret = secret
                account.key2 = key2
                account.secret2 = secret2
                account.name = name
                account.username = username
                account.status = status
                account.tags = tags

                db_session.add(account)

                try:
                    db_session.commit()
                    db_session.close()
                    flash('Record was successfully created.', 'success')
                    return redirect(url_for('autopostaccount.index_view'))
                except Exception as ex:
                    db_session.rollback()
                    db_session.close()
                    flash(str(ex), 'danger')

        return self.render('auto_post_account/create_tw.jinja2', form=form)

    @expose('/edit_tw', methods=['GET', 'POST'])
    def edit_tw(self):
        id_ = request.args.get('id', type=int, default=None)
        if not id_:
            abort(400)

        global db_engine
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()

        account = db_session.query(AutoPostAccount).filter(AutoPostAccount.id == id_).filter(
            AutoPostAccount.type == AUTO_POST_ACCOUNT_TYPE_TWITTER).first()
        if account is None:
            abort(404)

        form = TWForm(request.form)

        if request.method == 'GET':
            form.project.data = str(account.project_id)
            form.key.data = account.key
            form.secret.data = account.secret
            form.key2.data = account.key2
            form.secret2.data = account.secret2
            form.name.data = account.name
            form.username.data = account.username
            form.status.data = str(account.status)
            form.tags.data = account.tags

        if request.method == 'POST' and form.validate():
            project = form.project.data
            key = form.key.data
            secret = form.secret.data
            key2 = form.key2.data
            secret2 = form.secret2.data
            name = form.name.data
            username = form.username.data
            status = form.status.data
            tags = form.tags.data

            api_flag = False

            auth = tweepy.OAuthHandler(key, secret)
            auth.access_token = key2
            auth.access_token_secret = secret2
            api = tweepy.API(auth)

            try:
                api.me()
                api_flag = True
            except Exception as ex:
                flash(str(ex), 'danger')

            if api_flag:
                db_session_maker = sessionmaker(bind=db_engine)
                db_session = db_session_maker()
                account = db_session.query(AutoPostAccount).filter(AutoPostAccount.id == id_).first()

                account.project_id = project
                account.key = key
                account.secret = secret
                account.key2 = key2
                account.secret2 = secret2
                account.name = name
                account.username = username
                account.status = status
                account.tags = tags

                try:
                    db_session.commit()
                    db_session.close()
                    flash('Record was successfully saved.', 'success')
                    return redirect(url_for('autopostaccount.edit_tw', id=id_))
                except Exception as ex:
                    db_session.rollback()
                    db_session.close()
                    flash(str(ex), 'danger')

        return self.render('auto_post_account/edit_tw.jinja2', id=id_, form=form)

    list_template = 'auto_post_account/list.jinja2'
    column_list = ['project', 'name', 'type', 'status']
    can_create = False
    can_edit = False
    column_choices = {
        'type': [
            (AUTO_POST_ACCOUNT_TYPE_TWITTER, 'Twitter'),
            (AUTO_POST_ACCOUNT_TYPE_FACEBOOK, 'Facebook')
        ],
        'status': [
            (AUTO_POST_ACCOUNT_STATUS_ON, 'On'),
            (AUTO_POST_ACCOUNT_STATUS_OFF, 'Off')
        ]
    }
