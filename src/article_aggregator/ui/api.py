import json
from datetime import datetime

import pytz
from flask import Blueprint, Response, request, abort
from flask.views import MethodView
from sqlalchemy import Table, Column, Integer, String, DateTime, SmallInteger
from sqlalchemy import and_
from sqlalchemy import create_engine
from sqlalchemy import func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from slugify import slugify

from article_aggregator.config import config_get
from article_aggregator.models import Project, ProjectResult, Result

api_bp = Blueprint('api', __name__)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')

Base = declarative_base()


class ViewProjectResult(Base):
    __table__ = Table('view_project_results', Base.metadata,
                      Column('id', Integer, primary_key=True),
                      Column('project', Integer),
                      Column('result', Integer),
                      Column('result_source_id', Integer),
                      Column('result_source_title', String),

                      Column('result_title', String),
                      Column('result_summary', String),
                      Column('result_published', DateTime(timezone=True)),
                      Column('url', String),

                      Column('blocked', SmallInteger),

                      Column('created', DateTime(timezone=True))
                      )


class ResultsAll(MethodView):
    def get(self):
        ret_data = {}

        global db_uri
        db_engine = create_engine(db_uri)
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()

        filters = [ViewProjectResult.blocked == 0]

        until = request.args.get('until', type=int, default=None)
        if until:
            until_date = datetime.fromtimestamp(float(until), pytz.UTC)
            filters.append(ViewProjectResult.result_published < until_date)

        source = request.args.get('source', default='')
        if source:
            filters.append(ViewProjectResult.result_source_title == source)

        page_size = request.args.get('page_size', type=int, default=20)
        if page_size > 40:
            page_size = 20

        data = db_session.query(ViewProjectResult) \
            .filter(and_(*filters)) \
            .order_by(ViewProjectResult.result_published.desc()).limit(page_size).all()

        total_count = db_session.query(func.count(ViewProjectResult.id)).select_from(ViewProjectResult) \
            .filter(and_(*filters)) \
            .scalar()

        ret_data['total_count'] = total_count

        results = []

        for row in data:
            row_result = db_session.query(Result).filter(Result.id == row.result).first()
            if row_result is None:
                continue

            published = None
            if row_result.published is not None:
                published = row_result.published.astimezone(pytz.UTC).isoformat()

            project_slug = db_engine.execute(
                Project.__table__.select().with_only_columns([Project.slug]).where(Project.id == row.project)).scalar()
            path = '{}/{}-{}'.format(project_slug, slugify(row_result.title, to_lower=True), row.result)

            results.append({
                'id': row.result,
                'title': row_result.title,
                'summary': row_result.summary,
                'url': row_result.final_url,
                'image': row_result.image,
                'source': row.result_source_title,
                'published': published,
                'path': path
            })

        ret_data['results'] = results

        include_sources = request.args.get('include_sources', type=int, default=0)
        if include_sources:
            sources = []
            sources_raw = db_session.query(ViewProjectResult.result_source_title.label('title'),
                                           func.count(ViewProjectResult.result_source_title).label('cnt')) \
                .filter(and_(*filters)) \
                .group_by(ViewProjectResult.result_source_title) \
                .order_by(ViewProjectResult.result_source_title.label('cnt').desc()).all()

            for s in sources_raw:
                sources.append([s.title, s.cnt])

            ret_data['sources'] = sources

        resp = Response(response=json.dumps(ret_data),
                        status=200,
                        mimetype="application/json")

        db_session.close()
        db_engine.dispose()

        return resp


class Results(MethodView):
    def get(self):
        ret_data = {}

        global db_uri
        db_engine = create_engine(db_uri)
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()

        project_id = None
        project_slug = request.args.get('slug', default=None)
        project = db_session.query(Project).filter(Project.slug == project_slug).first()
        if project is not None:
            project_id = project.id

        filters = [ProjectResult.blocked == 0]

        if project_id:
            filters.append(ProjectResult.project_id == project_id)

        until = request.args.get('until', type=int, default=None)
        if until:
            until_date = datetime.fromtimestamp(float(until), pytz.UTC)
            filters.append(ProjectResult.result_published < until_date)

        source = request.args.get('source', default='')
        if source:
            filters.append(ProjectResult.result_source_title == source)

        page_size = request.args.get('page_size', type=int, default=20)
        if page_size > 40:
            page_size = 20

        data = db_session.query(ProjectResult) \
            .filter(and_(*filters)) \
            .order_by(ProjectResult.result_published.desc()).limit(page_size).all()

        total_count = db_session.query(func.count(ProjectResult.id)).select_from(ProjectResult) \
            .filter(and_(*filters)) \
            .scalar()

        ret_data['total_count'] = total_count

        results = []

        for row in data:
            published = None
            if row.result.published is not None:
                published = row.result.published.astimezone(pytz.UTC).isoformat()

            project_slug = db_engine.execute(
                Project.__table__.select().with_only_columns([Project.slug]).where(Project.id == row.project_id)).scalar()
            path = '{}/{}-{}'.format(project_slug, slugify(row.result.title, to_lower=True), row.result_id)

            results.append({
                'id': row.result_id,
                'title': row.result.title,
                'summary': row.result.summary,
                'url': row.result.final_url,
                'image': row.result.image,
                'source': row.result_source_title,
                'published': published,
                'path': path
            })

        ret_data['results'] = results

        include_sources = request.args.get('include_sources', type=int, default=0)
        if include_sources:
            sources = []
            sources_raw = db_session.query(ProjectResult.result_source_title.label('title'),
                                           func.count(ProjectResult.result_source_title).label('cnt')) \
                .filter(and_(*filters)) \
                .group_by(ProjectResult.result_source_title) \
                .order_by(ProjectResult.result_source_title.label('cnt').desc()).all()

            for s in sources_raw:
                sources.append([s.title, s.cnt])

            ret_data['sources'] = sources

        resp = Response(response=json.dumps(ret_data),
                        status=200,
                        mimetype="application/json")

        db_session.close()
        db_engine.dispose()

        return resp


class ProjectResultById(MethodView):
    def get(self):
        ret_data = {}

        project_slug = request.args.get('slug', default=None)
        result_id = request.args.get('id', type=int, default=None)
        if not project_slug or not result_id:
            abort(400)

        global db_uri
        db_engine = create_engine(db_uri)
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()

        project_id = None
        project = db_session.query(Project).filter(Project.slug == project_slug).first()
        if project is None:
            db_session.close()
            db_engine.dispose()
            abort(404)
        else:
            project_id = project.id

        filters = [ProjectResult.blocked == 0, ProjectResult.project_id == project_id,
                   ProjectResult.result_id == result_id]

        row = db_session.query(ProjectResult).filter(and_(*filters)).first()
        if row is None:
            status = 404
            ret_data['total_count'] = 0
            ret_data['result'] = {}
        else:
            status = 200
            published = None
            if row.result.published is not None:
                published = row.result.published.astimezone(pytz.UTC).isoformat()

            project_slug = db_engine.execute(
                Project.__table__.select().with_only_columns([Project.slug]).where(Project.id == row.project_id)).scalar()
            path = '{}/{}-{}'.format(project_slug, slugify(row.result.title, to_lower=True), row.result_id)

            ret_data['result'] = {
                'id': row.result_id,
                'title': row.result.title,
                'summary': row.result.summary,
                'url': row.result.final_url,
                'image': row.result.image,
                'source': row.result_source_title,
                'published': published,
                'path': path
            }

        resp = Response(response=json.dumps(ret_data),
                        status=status,
                        mimetype="application/json")

        db_session.close()
        db_engine.dispose()

        return resp


api_bp.add_url_rule('/api/results', view_func=Results.as_view('results'))
api_bp.add_url_rule('/api/results/all', view_func=ResultsAll.as_view('results_all'))
api_bp.add_url_rule('/api/result', view_func=ProjectResultById.as_view('result'))
