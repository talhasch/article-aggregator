$('input[name="token"]').attr('readonly', true);
$('input[name="token_secret"]').attr('readonly', true);
$('input[name="name"]').attr('readonly', true);

$('select[name="app"]').parent().append('<div style="padding: 10px; text-align: center"><input type="button" disabled class="btn btn-info btn-get-token" value="Get Token"></div>');


$('select[name="app"]').change(function () {
    activateTokenButton();
});

function activateTokenButton() {
    var selected_app = $('select[name="app"]').val();

    if (parseInt(selected_app)) {
        $('.btn-get-token').attr('disabled', false);
    } else {
        $('.btn-get-token').attr('disabled', true);
    }
}

activateTokenButton();

$('.btn-get-token').click(function () {
    var selected_app = $('select[name="app"]').val();
    var u = '/account/get_token?app=' + selected_app;
    window.open(u, 'get-token', 'width=600,height=500');
});

function callback(screen_name, access_token, access_token_secret) {
    $('input[name="name"]').val(screen_name);
    $('input[name="token"]').val(access_token);
    $('input[name="token_secret"]').val(access_token_secret);
}