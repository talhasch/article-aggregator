$(function () {

    $('#name').attr('readonly', true);
    $('#username').attr('readonly', true);

    $('#secret2').parents('.form-group').append('<br clear="all"> <div class="text-center" style="padding: 10px;">'+
            '<button class="btn btn-default btn-validate" type="button">Validate</button>'+
        '</div>');

    $('.btn-validate').click(function () {
        var key = $.trim($('#key').val());
        if (key == '') {
            $('#key').focus();
            return;
        }
        var secret = $.trim($('#secret').val());
        if (secret == '') {
            $('#secret').focus();
            return;
        }
        var key2 = $.trim($('#key2').val());
        if (key2 == '') {
            $('#key2').focus();
            return;
        }
        var secret2 = $.trim($('#secret2').val());
        if (secret2 == '') {
            $('#secret2').focus();
            return;
        }

        var self = $(this);
        var self_text = $(this).text();

         $(this).text('...').attr('disabled', true);

        var u = '/autopostaccount/validate_tw?key=' + key + '&secret=' + secret+ '&key2=' + key2+ '&secret2=' + secret2;
        $.getJSON(u, function(resp){
            if(resp.status==0){
                alert(resp.message)
            } else {
                $('#name').val(resp.screen_name);
                $('#username').val(resp.id);
            }
        }).always(function(){
            self.text(self_text).attr('disabled', false);
        });
    });
});