$.getJSON('/project/categories', function (response) {
    // local dataset
    var category_list = response;

    // Constructing the suggestion engine
    var categories = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: category_list
    });

    // Initializing the typeahead
    $('input[id="source_category"]').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'categories',
            source: categories
        });
});

function show_results(q, c) {
    $('#modal-results').modal();
    var u = '/?inline=1&query=' + q + '&ds_cat=' + c;
    $('#iframe-results').attr('src', u);
}

$('body').on('click', '.btn-result', function () {
    var parent = $(this).parents('.inline-form-field').eq(0);
    var txtQuery = parent.find('input[type="text"]').eq(0);
    if ($.trim(txtQuery.val()) == '') {
        txtQuery.focus();
        return false;
    }
    var q = txtQuery.val();
    var c = $.trim($('input[id="source_category"]').val());
    show_results(q, c);
});

function insertResultsBtn() {
    var btnh = '<div style="text-align: right"><button type="button" class="btn btn-primary btn-result">Show Results</button></div>';

    var fields = $('.inline-field-list .inline-field');
    for (var c = 0; c < fields.length; c++) {
        var elem = $(fields[c]);
        if (!elem.hasClass('btn-done')) {
            elem.find('.inline-form-field').append(btnh);
            elem.addClass('btn-done');
        }
    }
}

window.onload = function () {
    setInterval(function () {
        insertResultsBtn();
    }, 500);
};

insertResultsBtn();