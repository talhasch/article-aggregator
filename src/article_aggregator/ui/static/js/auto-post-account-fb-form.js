$(function () {

    $('#key2').attr('readonly', true);
    $('#name').attr('readonly', true);
    $('#username').attr('readonly', true);

    $('#secret').parents('.form-group').append('<br clear="all"> <div class="text-center" style="padding: 10px;">'+
            '<button class="btn btn-default btn-get-token" type="button">Select Page</button>'+
        '</div>');

    $('.btn-get-token').click(function () {
        var key = $.trim($('#key').val());
        if (key == '') {
            $('#key').focus();
            return;
        }
        var secret = $.trim($('#secret').val());
        if (secret == '') {
            $('#secret').focus();
            return;
        }
        var u = '/autopostaccount/get_fb_page_token?key=' + key + '&secret=' + secret;
        window.open(u, 'get-page-token', 'width=600,height=500');
    });
});

function callback(id, name, token) {
    $('#key2').val(token);
    $('#username').val(id);
    $('#name').val(name);
}