$.getJSON('/datasource/groups', function (response) {
    // local dataset
    var group_list = response;

    // Constructing the suggestion engine
    var groups = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: group_list
    });

    // Initializing the typeahead
    $('input[id="group"]').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'groups',
            source: groups
        });
});

$.getJSON('/datasource/categories', function (response) {
    // local dataset
    var category_list = response;

    // Constructing the suggestion engine
    var categories = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: category_list
    });

    // Initializing the typeahead
    $('input[id="category"]').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'categories',
            source: categories
        });
});

function disable_form() {
    $('form[method="POST"]').find('input,select').attr('disabled', true);
}

function enable_form() {
    $('form[method="POST"]').find('input,select').attr('disabled', false);
}

function getEndpoint() {
    var type = $('select[id="type"]').val();
    var endpoint = '';
    if (type == '1') {
        endpoint = '/datasource/rss';
    }
    if (type == '2') {
        endpoint = '/datasource/twitter';
    }
    if (type == '3') {
        endpoint = '/datasource/facebook';
    }

    return endpoint
}

function getInfo() {
    var address = $.trim($('input[id="address"]').val());
    if (address == '') {
        return;
    }

    var endpoint = getEndpoint();

    // $('input[id="title"]').val('');
    // $('input[id="filters"]').val('');
    disable_form();
    $.post(endpoint, {address: address}, function (response) {
        if (response.status == 1) {
            if ($.trim($('input[id="title"]').val()) == '') {
                $('input[id="title"]').val(response.data.title);
            }

            if ($.trim($('input[id="filters"]').val()) == '') {
                $('input[id="filters"]').val(response.data.domain);
            }
        }
    }, 'json').always(function () {
        enable_form();
    });
}

$('input[id="address"]').blur(function () {
    getInfo();
});

$('select[id="type"]').change(function () {
    getInfo();
});

$('.btn-test').click(function () {
    var address = $.trim($('input[id="address"]').val());
    if (address == '') {
        $('input[id="address"]').focus();
        return;
    }

    var endpoint = getEndpoint();

    disable_form();
    $.post(endpoint, {address: address}, function (response) {
        $('#modal-test .source-title, #modal-test .source-list').html('');
        $('#modal-test').modal();

        if (response.status == 0) {
            $('#modal-test .source-list').append('Not a valid address!');
        }

        if (response.status == 1) {
            $('#modal-test .source-title').html(response.data.title);
            for (var i = 0; i < response.data.items.length; i += 1) {
                var item = response.data.items[i];
                $('#modal-test .source-list').append('<div class="item"><a class="link" href="' + item.link + '" target="_blank">' + item.link + '</a><span class="published">' + item.published + '</span></div>');
            }
        }
        if (response.status == 2) {
            $('#modal-test .source-list').html('<h5>Server Error</h5>' + response.msg);
        }
    }, 'json').always(function () {
        enable_form();
    });
});

if(typeof d_id != 'undefined'){
    $('.btn-reset').click(function(){

        if (!confirm('Are you sure?')) {
            return;
        }
        var u = '/datasource/reset?id='+d_id;
        location.href = u;
    });

    $('.show-results').click(function(event){
        event.preventDefault();
        $('#modal-results').modal();
        var u = '/?inline=1&ds=' + d_id;
        $('#iframe-results').attr('src', u);
    })
}
