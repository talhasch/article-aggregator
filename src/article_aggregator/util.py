from datetime import datetime


def now_utc():
    import pytz
    return datetime.utcnow().replace(tzinfo=pytz.utc)


