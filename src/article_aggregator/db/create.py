import logging

from sqlalchemy import create_engine

from article_aggregator.config import config_get
from article_aggregator.models import *

logging.basicConfig(level=logging.INFO)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


def create_db():
    db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')
    db_engine = create_engine(db_uri)
    Base.metadata.create_all(bind=db_engine)

    ddl = """CREATE OR REPLACE FUNCTION after_datasource_deleted() RETURNS TRIGGER AS $$
    BEGIN
        UPDATE {} SET status=15 WHERE source is NULL AND status!=15 ;
        RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;""".format(Result.__table__)
    db_engine.engine.execute(ddl)

    ddl = """CREATE TRIGGER tr_after_datasource_deleted AFTER DELETE ON {}
    FOR EACH ROW EXECUTE PROCEDURE after_datasource_deleted();""".format(DataSource.__table__)
    db_engine.engine.execute(ddl)

    ddl = """
    CREATE or REPLACE VIEW view_project_results as
    SELECT
        DISTINCT ON
        (
            result,
            result_published
        ) *
    FROM
        project_results
    WHERE blocked=0
    ORDER BY
        result_published DESC limit 1000"""
    db_engine.engine.execute(ddl)

    logging.info("OK")


def main():
    create_db()


if __name__ == "__main__":
    main()
