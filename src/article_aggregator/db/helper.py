from article_aggregator.constants import *
from article_aggregator.util import now_utc


def get_next_twitter_account():
    from article_aggregator.models import App, Account
    from article_aggregator.config import config_get
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker

    db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')
    db_engine = create_engine(db_uri)

    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()

    account_data = db_session.query(Account).outerjoin(App, Account.app_id == App.id) \
        .filter(App.type == APP_TYPE_TWITTER) \
        .filter(App.status == APP_STATUS_ON) \
        .filter(Account.status == ACCOUNT_STATUS_ON) \
        .order_by(Account.last_used.asc()).limit(1).first()

    ret_val = {}
    if account_data is not None:
        ret_val['id'] = account_data.id
        ret_val['name'] = account_data.name
        ret_val['app_key'] = account_data.app.key
        ret_val['app_secret'] = account_data.app.secret
        ret_val['token'] = account_data.token
        ret_val['token_secret'] = account_data.token_secret
        account_data.last_used = now_utc()

    db_session.commit()
    db_session.close()
    db_engine.dispose()

    return ret_val


def get_next_facebook_account():
    from article_aggregator.models import App, Account
    from article_aggregator.config import config_get
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker

    db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')
    db_engine = create_engine(db_uri)

    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()

    account_data = db_session.query(Account).outerjoin(App, Account.app_id == App.id) \
        .filter(App.type == APP_TYPE_FACEBOOK) \
        .filter(App.status == APP_STATUS_ON) \
        .filter(Account.status == ACCOUNT_STATUS_ON) \
        .order_by(Account.last_used.asc()).limit(1).first()

    ret_val = {}
    if account_data is not None:
        ret_val['id'] = account_data.id
        ret_val['name'] = account_data.name
        ret_val['app_key'] = account_data.app.key
        ret_val['app_secret'] = account_data.app.secret
        ret_val['token'] = account_data.token
        account_data.last_used = now_utc()

    db_session.commit()
    db_session.close()
    db_engine.dispose()

    return ret_val
