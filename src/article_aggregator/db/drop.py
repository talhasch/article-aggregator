import logging
import sys

from article_aggregator.config import config_get
from article_aggregator.models import *
from sqlalchemy import create_engine

logging.basicConfig(level=logging.INFO)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


def drop_db():
    confirm = input('All data will be deleted. Are you sure? Y or N: ')

    while confirm not in ['Y', 'N']:
        confirm = input('Y or N: ')

    if confirm == 'N':
        logging.info('Aborting.')
        sys.exit(0)

    db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')
    db_engine = create_engine(db_uri)

    db_engine.execute('DROP VIEW view_project_results')

    Base.metadata.drop_all(bind=db_engine)
    logging.info("OK")


def main():
    drop_db()


if __name__ == "__main__":
    main()
