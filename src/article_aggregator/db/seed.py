import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from article_aggregator.config import config_get
from article_aggregator.models import DataSource, App, Account
from article_aggregator.constants import *

logging.basicConfig(level=logging.INFO)


def create_app():
    data = [
        {
            "id": 1,
            "type": 1,
            "name": "FC Exclusive - Twitter",
            "key": "NWJU33g15aGlQb1SutADGSPL7",
            "secret": "4JmRZPIkMOJiM2AC6Lamk8aUIudGvFCa97QVIvQf5klFikB7KZ",
            "status": 1
        },
        {
            "id": 2,
            "type": 2,
            "name": "FC Exclusive - Facebook",
            "key": "873734712720094",
            "secret": "979944745aa1f65cdfea1a73fc048cae",
            "status": 1
        }
    ]

    db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')
    db_engine = create_engine(db_uri)

    Session = sessionmaker(bind=db_engine)
    db_session = Session()

    db_session.bulk_insert_mappings(App, data)
    db_session.commit()
    logging.info("APP OK")
    db_session.close()


def create_account():
    data = [
        {
            "id": 1,
            "app_id": 1,
            "name": "FCExclusive",
            "token": "3145162006-4wSemoehWamSMpN5m6y75l4Ngrr55Lg7MjubzvV",
            "token_secret": "ZEol5VEDlOMct3snvl5sJHHs2XUX7lzAR0hm90FrBTUKP",
            "status": 1
        },
        {
            "id": 2,
            "app_id": 1,
            "name": "chris_bolt",
            "token": "251194059-3j0qyk7Urm9VKZzGSEjlMw6D2AVPO5mmibp6TFHv",
            "token_secret": "raa75gtjnWofT43uVh6mPej6mXQNLMQYNTajaDFMRGntd",
            "status": 1
        },
        {
            "id": 3,
            "app_id": 1,
            "name": "talhasch",
            "token": "19641817-i66bjW67GJJciU9AkQ8cQR3E5jC0Ad8Y2kqtlfRsY",
            "token_secret": "ID1axRWgLqKn6jhraGH87VlqqK9NbogNZ87hi7Va2WX9E",
            "status": 1
        },
        {
            "id": 4,
            "app_id": 1,
            "name": "FCEPortVale",
            "token": "4894324857-17tKg8S8GgfkBLGbrbiemggLkUFUWJtMfjUQUUg",
            "token_secret": "F5ETe7Kw8YevpYUkND88vMSptUYDGNtGrXNLoYnjua9D4",
            "status": 1
        },
        {
            "id": 5,
            "app_id": 1,
            "name": "FCEStokeCity",
            "token": "763387266953871360-NiJxNmriyGbbA4ybxKFBDblhp2arokQ",
            "token_secret": "ZpIzII8u4iQKyNEFz56ulMT1qdgB8OpEt5eiyCNGp2dF3",
            "status": 1
        },
        {
            "id": 6,
            "app_id": 1,
            "name": "FCEArsenal",
            "token": "767739704875909121-Gra0rgcosEXK6xfCxjHmj42nmL4h9aa",
            "token_secret": "BmvyO2HxbKGdIiCj2oKK8gpUYOzKNTk8NnvXzSTCgLy0B",
            "status": 1
        },
        {
            "id": 7,
            "app_id": 1,
            "name": "FCEBurnley",
            "token": "767780378404917248-D37jNsKa4tXXAQxrqSsmgOEANfoLojx",
            "token_secret": "ZfCCZQWpAohF3L0nt07R2ioieOteCKtqe6Pi4mNkWGl9a",
            "status": 1
        },
        {
            "id": 8,
            "app_id": 1,
            "name": "FCEChelsea",
            "token": "767781561190211584-3fuAm8UhFtv9Ha1N2I62TyLyNV0X5e6",
            "token_secret": "1iIZWB2wVYWam8xqJreYeXkCMfmeE42S8cnB40YNSsoCp",
            "status": 1
        },
        {
            "id": 9,
            "app_id": 1,
            "name": "FCEPalace",
            "token": "767783132171014144-fOsltfdaSx21wpCtcKSwN3bsUpLPwMI",
            "token_secret": "OZYocb8LVRGKim6kx4FcGjL2V2hyxxaZ2IlfCFFJcXfdN",
            "status": 1
        },
        {
            "id": 10,
            "app_id": 1,
            "name": "FCE_Everton",
            "token": "767786630824529920-9BVP9EliN7y6hPOPFGPFaPCkpe0JtM5",
            "token_secret": "QsZpqjUJkao5cZMNhebWUS2DjOejKZP2JP8PO9erPtj6j",
            "status": 1
        },
        {
            "id": 11,
            "app_id": 1,
            "name": "FCEHullcity",
            "token": "773527648014303232-tcN0qgi6OegWyncdhQG7fv3ywJriSXh",
            "token_secret": "RMajHAHzvGXV7O8NJpYGexRYrKqRCi8z31m0bp22wwNP2",
            "status": 1
        },
        {
            "id": 12,
            "app_id": 2,
            "name": "Chris Bolt",
            "token": "EAAMaqDctZBt4BAAzxwBpgZCoh83t5ZAyfeXdhOUatsbjPQ6fM2YqHoFCbpR2qxtjFW55tzwhrJLvwnCE195Fju26ZCjMZCjofZCSWb6Fox0apLrJxPYCKkKrEgLEFgZBQSzFpWlTYwGQmSvV7c5vnoUljvow7Tdd1AZD",
            "token_secret": None,
            "status": 1
        },
        {
            "id": 13,
            "app_id": 2,
            "name": "Leni Yoa",
            "token": "EAAMaqDctZBt4BABmufXo5dK20BTGWvfDT2jgILiP8HceISkRINPxWKfE4UYqFw0QrB23mvML0wpZCGiPZCZBFnEOyiDeX2xwmLUeVnDnw7etV23Xlhg7tSky0gnx5yyPkNVItVjDj06G3P7AirAesSjinpMZCCjIZD",
            "token_secret": None,
            "status": 1
        },
        {
            "id": 14,
            "app_id": 2,
            "name": "Talha Buğra Bulut",
            "token": "EAAMaqDctZBt4BAP48oiRLzqZAvvzQhXXGHay95Qn9NooZCkWY2ZBnot0Pp9ZAOMFLQnTTJFdZCsAuCNgNlG8MEZB12Bda0L7rPbE3ZCnCZAZACrGkCru6pXsVHAZAVsPCrGwaZBLBVEi2EI4MNyjDIeHEjqhy0bBtgYYwkQZD",
            "token_secret": None,
            "status": 1
        }
    ]

    db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')
    db_engine = create_engine(db_uri)

    Session = sessionmaker(bind=db_engine)
    db_session = Session()

    db_session.bulk_insert_mappings(Account, data)
    db_session.commit()
    logging.info("ACCOUNT OK")
    db_session.close()


def create_data_source():
    data = [
        {
            "type": 2,
            "category": "mixed",
            "group": "skysports",
            "title": "Sky Sports",
            "address": "SkySports",
            "filters": "skysports"
        },
        {
            "type": 3,
            "category": "mixed",
            "group": "skysports",
            "title": "Sky Sports",
            "address": "SkySports",
            "filters": "skysports"
        },
        {
            "type": 1,
            "category": "arsenal",
            "group": "arsenal-com",
            "title": "Arsenal FC",
            "address": "http://feeds.arsenal.com/arsenal-news",
            "filters": "arsenal"
        },
        {
            "type": 1,
            "category": "arsenal",
            "group": "arseblog",
            "title": "Arseblog",
            "address": "http://arseblog.com/feed/",
            "filters": "arseblog"
        },
        {
            "type": 1,
            "category": "mixed",
            "group": "dailymail",
            "title": "Daily Mail",
            "address": "http://www.dailymail.co.uk/sport/index.rss",
            "filters": "dailymail"
        },
        {
            "type": 1,
            "category": "mixed",
            "group": "90min",
            "title": "90min",
            "address": "http://www.90min.com/posts.rss",
            "filters": "90min"
        },
        {
            "type": 1,
            "category": "mixed",
            "group": "islingtongazette",
            "title": "Islington Gazette",
            "address": "http://www.islingtongazette.co.uk/cmlink/sport_football_1_3818712",
            "filters": "islingtongazette"
        },
        {
            "type": 3,
            "category": "mixed",
            "group": "footballfancast",
            "title": "Football FanCast",
            "address": "FootballFanCast",
            "filters": "footballfancast"
        },
        {
            "type": 1,
            "category": "mixed",
            "group": "thesportreview",
            "title": "The Sport Review",
            "address": "http://feeds.feedburner.com/thesportreview",
            "filters": "thesportreview"
        },
        {
            "type": 2,
            "category": "mixed",
            "group": "telegraph",
            "title": "The Telegraph",
            "address": "TelegraphSport",
            "filters": "telegraph"
        },
        {
            "type": 1,
            "category": "mixed",
            "group": "theguardian",
            "title": "The Guardian",
            "address": "https://www.theguardian.com/uk/sport/rss",
            "filters": "theguardian"
        },
        {
            "type": 1,
            "category": "mixed",
            "group": "westlondonsport",
            "title": "West London Sport",
            "address": "http://www.westlondonsport.com/feed",
            "filters": "westlondonsport"
        },
        {
            "type": 1,
            "category": "mixed",
            "group": "bbc",
            "title": "BBC",
            "address": "http://feeds.bbci.co.uk/sport/football/rss.xml",
            "filters": "bbc"
        },
        {
            "type": 1,
            "category": "mixed",
            "group": "talksport",
            "title": "talkSPORT",
            "address": "http://talksport.com/taxonomy/term/2034/all/feed",
            "filters": "talksport"
        },
        {
            "type": 1,
            "category": "mixed",
            "group": "goal-com",
            "title": "Goal.com",
            "address": "http://www.goal.com/en-gb/feeds/news?fmt=rss",
            "filters": "goal"
        }
    ]

    db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')
    db_engine = create_engine(db_uri)

    Session = sessionmaker(bind=db_engine)
    db_session = Session()

    try:
        db_session.bulk_insert_mappings(DataSource, data)
        db_session.commit()
        logging.info("DATA SOURCE OK")
    except Exception as ex:
        db_session.rollback()
        logging.error("DATA SOURCE FAIL")

    db_session.close()


def main():
    create_app()
    create_account()
    create_data_source()


if __name__ == "__main__":
    main()
