import logging
import time
from datetime import timedelta

from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker

from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.models import JobQueue, Result
from article_aggregator.util import now_utc

logging.basicConfig(level=logging.INFO)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')


def recover_parse_tasks():
    global db_uri
    while True:
        db_engine = create_engine(db_uri)
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()

        types = (JOB_TYPE_PARSE,)
        min_date = now_utc() - timedelta(minutes=3)

        try:
            jobs = db_session.query(JobQueue).with_for_update(nowait=True, of=JobQueue) \
                .filter(JobQueue.status == JOB_QUEUE_STATUS_LOCKED) \
                .filter(JobQueue.type.in_(types)) \
                .filter(JobQueue.executed_date < min_date) \
                .order_by(JobQueue.executed_date.asc()).limit(100).all()
        except OperationalError as ex:
            jobs = []

        if len(jobs) == 0:
            db_session.close()
            db_engine.dispose()
            time.sleep(10)
            continue

        for job in jobs:
            if job.try_count >= 5:
                logging.info("Max try count reached")
                db_session.delete(job)
            else:
                if job.type == JOB_TYPE_PARSE:
                    result_id = job.arg1
                    the_result = db_session.query(Result).filter(Result.id == result_id).filter(
                        Result.status == RESULT_STATUS_PARSE_FAILED).first()
                    if the_result is not None:
                        the_result.status = RESULT_STATUS_PARSING

                job.execute_date = now_utc()
                job.status = JOB_QUEUE_STATUS_ON
                logging.info("Job {} recovered".format(job.id))

        db_session.commit()
        db_session.close()
        db_engine.dispose()


def recover_scrape_tasks():
    global db_uri
    while True:
        db_engine = create_engine(db_uri)
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()

        types = (JOB_TYPE_SCRAPE,)
        min_date = now_utc() - timedelta(minutes=10)

        try:
            jobs = db_session.query(JobQueue).with_for_update(nowait=True, of=JobQueue) \
                .filter(JobQueue.status == JOB_QUEUE_STATUS_LOCKED) \
                .filter(JobQueue.type.in_(types)) \
                .filter(JobQueue.executed_date < min_date) \
                .order_by(JobQueue.executed_date.asc()).limit(100).all()
        except OperationalError as ex:
            jobs = []

        if len(jobs) == 0:
            db_session.close()
            db_engine.dispose()
            time.sleep(10)
            continue

        for job in jobs:
            if job.try_count >= 5:
                logging.info("Max try count reached")
                db_session.delete(job)
            else:

                job.execute_date = now_utc()
                job.status = JOB_QUEUE_STATUS_ON
                logging.info("Job {} recovered".format(job.id))

        db_session.commit()
        db_session.close()
        db_engine.dispose()


def main():
    recover_parse_tasks()
    recover_scrape_tasks()
    time.sleep(10)


if __name__ == '__main__':
    main()
