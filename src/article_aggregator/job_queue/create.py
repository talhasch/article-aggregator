import logging
import time
from datetime import timedelta

from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import nullsfirst

from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.models import DataSource, JobQueue, Result
from article_aggregator.util import now_utc

logging.basicConfig(level=logging.INFO)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')


def create_scrape_tasks():
    global db_uri
    db_engine = create_engine(db_uri)
    db_session_maker = sessionmaker(bind=db_engine)
    db_session_read = db_session_maker()
    db_session_write = db_session_maker()

    min_date = now_utc() - timedelta(minutes=5)
    try:
        data_sources = db_session_read.query(DataSource).with_for_update(nowait=True, of=DataSource). \
            filter(DataSource.status == DATASOURCE_STATUS_ON). \
            filter(DataSource.last_triggered < min_date). \
            order_by(nullsfirst(DataSource.last_triggered.asc())).limit(100).all()
    except OperationalError as ex:
        data_sources = []

    new_jobs = 0
    for data_source in data_sources:
        data_source.last_triggered = now_utc()

        job = JobQueue()
        job.type = JOB_TYPE_SCRAPE
        job.arg1 = data_source.id
        job.execute_date = now_utc()
        db_session_write.add(job)
        new_jobs += 1

    db_session_read.commit()
    db_session_write.commit()

    if new_jobs > 0:
        logging.info('{} job(s) created for {}'.format(new_jobs, 'JOB_TYPE_SCRAPE'))

    db_session_read.close()
    db_session_write.close()
    db_engine.dispose()


def create_parse_tasks():
    global db_uri
    db_engine = create_engine(db_uri)
    db_session_maker = sessionmaker(bind=db_engine)
    db_session_read = db_session_maker()
    db_session_write = db_session_maker()

    try:
        results = db_session_read.query(Result).with_for_update(nowait=True, of=Result). \
            filter(Result.status == RESULT_STATUS_NEW). \
            order_by(nullsfirst(Result.published.asc())).limit(40).all()
    except OperationalError as ex:
        results = []

    new_jobs = 0
    for result in results:
        # data_source.last_triggered = now_utc()
        result.status = RESULT_STATUS_PARSING

        job = JobQueue()
        job.type = JOB_TYPE_PARSE
        job.arg1 = result.id
        job.execute_date = now_utc()
        db_session_write.add(job)
        new_jobs += 1

    db_session_read.commit()
    db_session_write.commit()

    if new_jobs > 0:
        logging.info('{} job(s) created for {}'.format(new_jobs, 'JOB_TYPE_PARSE'))

    db_session_read.close()
    db_session_write.close()
    db_engine.dispose()


def create_parse_tasks_parsing():
    global db_uri
    db_engine = create_engine(db_uri)
    db_session_maker = sessionmaker(bind=db_engine)
    db_session_read = db_session_maker()
    db_session_write = db_session_maker()

    min_date = now_utc() - timedelta(minutes=10)

    results = db_session_read.query(Result). \
        filter(Result.status == RESULT_STATUS_PARSING). \
        filter(Result.created < min_date). \
        order_by(nullsfirst(Result.published.asc())).limit(100).all()

    new_jobs = 0
    for result in results:
        job_check = db_session_read.query(JobQueue).filter(JobQueue.arg1 == str(result.id)).filter(
            JobQueue.type == JOB_TYPE_PARSE).first()

        if job_check is not None:
            continue

        job = JobQueue()
        job.type = JOB_TYPE_PARSE
        job.arg1 = result.id
        job.execute_date = now_utc()
        db_session_write.add(job)
        new_jobs += 1

    db_session_read.commit()
    db_session_write.commit()

    if new_jobs > 0:
        logging.info('{} job(s) created for {}'.format(new_jobs, 'JOB_TYPE_PARSE'))

    db_session_read.close()
    db_session_write.close()
    db_engine.dispose()


def main():
    while True:
        create_scrape_tasks()
        create_parse_tasks()
        create_parse_tasks_parsing()
        time.sleep(5.0)


if __name__ == '__main__':
    main()
