import logging
import time

from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.models import JobQueue
from article_aggregator.util import now_utc
from article_aggregator.tasks.run import scrape_task, parse_task
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker

logging.basicConfig(level=logging.INFO)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')


def main():
    global db_uri
    while True:
        db_engine = create_engine(db_uri)
        db_session_maker = sessionmaker(bind=db_engine)
        db_session = db_session_maker()

        types = (JOB_TYPE_SCRAPE, JOB_TYPE_PARSE)

        try:
            jobs = db_session.query(JobQueue).with_for_update(nowait=True, of=JobQueue) \
                .filter(JobQueue.status == JOB_QUEUE_STATUS_ON) \
                .filter(JobQueue.type.in_(types)) \
                .filter(JobQueue.execute_date < now_utc()) \
                .order_by(JobQueue.execute_date.asc()).limit(200).all()
        except OperationalError as ex:
            jobs = []

        if len(jobs) == 0:
            db_session.close()
            db_engine.dispose()
            time.sleep(1)
            continue

        run_jobs = 0
        for job in jobs:
            if job.type == JOB_TYPE_SCRAPE:
                res = scrape_task.delay(job.id)
                run_jobs += 1

            if job.type == JOB_TYPE_PARSE:
                res = parse_task.delay(job.id)
                run_jobs += 1

            job.executed_date = now_utc()
            job.status = JOB_QUEUE_STATUS_LOCKED
            job.try_count += 1

        db_session.commit()

        if run_jobs > 0:
            logging.info('{} job(s) run '.format(run_jobs))

        db_session.close()
        db_engine.dispose()
        time.sleep(0.5)


if __name__ == '__main__':
    main()
