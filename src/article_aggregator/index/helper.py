def make_index_config(index_type):
    return {
        'settings': {
            'index': {
                'max_result_window': 2147483647
            },
            'analysis': {
                'analyzer': {
                    'index_analyzer': {
                        'tokenizer': 'whitespace',
                        'filter': ['standard', 'lowercase', 'asciifolding']
                    }
                }
            }
        },
        'mappings': {
            index_type: {
                '_all': {'enabled': False},
                'properties': {
                    'id': {
                        'type': 'long',
                        'index': 'not_analyzed'
                    },
                    'source': {
                        'properties': {
                            'id': {
                                'type': 'integer',
                                'index': 'not_analyzed',

                            },
                            'category': {
                                'type': 'string',
                                'index': 'not_analyzed'
                            },
                            'group': {
                                'type': 'string',
                                'index': 'not_analyzed'
                            },
                            'title': {
                                'type': 'string',
                                'index': 'not_analyzed'
                            }
                        }
                    },
                    'url': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    },
                    'title': {
                        'type': 'string',
                        'index': 'analyzed',
                        'analyzer': 'index_analyzer'
                    },
                    'summary': {
                        'type': 'string',
                        'index': 'analyzed',
                        'analyzer': 'index_analyzer'
                    },
                    'image': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    },
                    'published': {
                        'type': 'date',
                        'index': 'not_analyzed'
                    }
                }
            }
        }
    }


es_escape_rules = {
    '&': r'\&',
    '|': r'\|',
    '!': r'\!',
    '{': r'\{',
    '}': r'\}',
    '[': r'\[',
    ']': r'\]',
    '^': r'\^',
    '~': r'\~',
    '?': r'\?',
    ':': r'\:',
    ';': r'\;'
}


def make_search_body(query_str, data_source_id=None, data_source_category=None, min_date=None, start_from=0, page_size=20):
    from datetime import datetime
    import re

    ds_regexp = " ds:([^\s]+)"
    regexp = re.compile(ds_regexp, re.IGNORECASE)
    ds_search = re.search(regexp,  query_str)
    if ds_search is not None:
        ds_part = ds_search.group(0)
        ds_part_split = ds_part.split(':')
        ds_id = ds_part_split[-1]
        if ds_id.isdigit():
            data_source_id = ds_id

        query_str = re.sub(regexp, " ", query_str)

    search_body = {
        "query": {
            "filtered": {
                "query": {
                    "match_all": {}
                },
                "filter": {"bool": {"must": []}}
            }
        },
        'sort': {
            'published': 'desc'
        },
        'from': start_from,
        'size': page_size
    }

    if query_str:
        search_body['query']['filtered']['query'] = {
            'query_string': {
                'fields': [
                    'title',
                    'summary'
                ],
                'default_operator': 'AND',
                'query': es_escape_arg(query_str)
            }
        }

        search_body['highlight'] = {
            'pre_tags': ['<mark>'],
            'post_tags': ['</mark>'],
            'fields': {'title': {'fragment_size': 2000, 'number_of_fragments': 3},
                       'summary': {'fragment_size': 10000, 'number_of_fragments': 3}}
        }

    if data_source_id:
        search_body['query']['filtered']['filter']['bool']['must'].append({'term': {'source.id': data_source_id}})

    if data_source_category:
        search_body['query']['filtered']['filter']['bool']['must'].append({'term': {'source.category': data_source_category}})

    if type(min_date) == datetime:
        str_date = min_date.strftime('%Y-%m-%dT%H:%M:%S.%f%z')
        search_body['query']['filtered']['filter']['bool']['must'].append({'range': {'published': {'gte': str_date}}})

    return search_body


def es_escaped_seq(term):
    for char in term:
        if char in es_escape_rules.keys():
            yield es_escape_rules[char]
        else:
            yield char


def es_escape_arg(term):
    term = term.replace('\\', r'\\')
    return "".join([nextStr for nextStr in es_escaped_seq(term)])
