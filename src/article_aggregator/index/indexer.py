import logging
import time
import pytz

from elasticsearch import Elasticsearch
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import StaleDataError

from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.models import Result

logging.basicConfig(level=logging.INFO)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')

es_host = config_get('ES', 'HOST')
index_name = config_get('ES', 'INDEX_NAME')
index_type = config_get('ES', 'TYPE_NAME')
es = Elasticsearch(es_host)


def index():
    global db_uri, es

    db_engine = create_engine(db_uri)

    Session = sessionmaker(bind=db_engine)
    db_session = Session()

    results = db_session.query(Result).filter(
        Result.status.in_((RESULT_STATUS_PARSED, RESULT_STATUS_REINDEX))).order_by(
        Result.published.asc()).limit(50).all()

    if len(results) == 0:
        db_session.close()
        db_engine.dispose()
        return

    indexed = 0
    for result in results:
        doc = {
            'id': result.id,
            'source': {
                'id': result.source.id,
                'category': result.source.category,
                'group': result.source.group,
                'title': result.source.title,
                'address': result.source.address,
                'type': result.source.type
            },
            'url': result.final_url,
            'title': result.title,
            'summary': result.summary,
            'image': result.image,
            'published': result.published.astimezone(pytz.UTC)
        }

        es.index(index=index_name, doc_type=index_type, id=result.id, body=doc)
        result.status = RESULT_STATUS_INDEXED
        indexed += 1

    try:
        db_session.commit()
    except StaleDataError as ex:
        db_session.rollback()

    db_session.close()
    db_engine.dispose()

    logging.info('{} result(s) indexed '.format(indexed))


def main():
    while True:
        index()
        time.sleep(2)


if __name__ == '__main__':
    main()
