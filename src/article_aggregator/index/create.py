import logging

from elasticsearch import Elasticsearch

from article_aggregator.config import config_get
from article_aggregator.index.helper import make_index_config

logging.basicConfig(level=logging.INFO)

es_host = config_get('ES', 'HOST')
index_name = config_get('ES', 'INDEX_NAME')
index_type = config_get('ES', 'TYPE_NAME')

es = Elasticsearch(es_host)


def main():
    global es
    index_config = make_index_config(index_type)
    es.indices.create(index=index_name, body=index_config)
    logging.info("OK")


if __name__ == "__main__":
    main()
