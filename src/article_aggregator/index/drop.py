import logging
import sys

from elasticsearch import Elasticsearch

from article_aggregator.config import config_get

logging.basicConfig(level=logging.INFO)

es_host = config_get('ES', 'HOST')
index_name = config_get('ES', 'INDEX_NAME')

es = Elasticsearch(es_host)


def main():
    confirm = input('Index will be deleted. Are you sure? Y or N: ')

    while confirm not in ['Y', 'N']:
        confirm = input('Y or N: ')

    if confirm == 'N':
        logging.info('Aborting.')
        sys.exit(0)

    global es

    if es.indices.exists(index_name):
        es.indices.delete(index_name)
        logging.info("OK")
    else:
        logging.error("Index not exists!")


if __name__ == "__main__":
    main()
