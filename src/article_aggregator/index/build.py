import logging
import time

import pytz
from elasticsearch import Elasticsearch, helpers
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.models import Result

logging.basicConfig(level=logging.INFO)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')


es_host = config_get('ES', 'HOST')
index_name = config_get('ES', 'INDEX_NAME')
index_type = config_get('ES', 'TYPE_NAME')
es = Elasticsearch(es_host)


def main():
    global db_uri, es

    db_engine = create_engine(db_uri)

    Session = sessionmaker(bind=db_engine)
    db_session = Session()

    min_id = 0
    while True:
        logging.info('min_id: %s' % min_id)
        results = db_session.query(Result).filter(
            Result.status.in_((RESULT_STATUS_PARSED, RESULT_STATUS_INDEXED, RESULT_STATUS_REINDEX))).filter(
            Result.id > min_id).order_by(
            Result.id.asc()).limit(50).all()

        if len(results) == 0:
            break

        index_data = []
        for result in results:
            index_data.append({
                '_index': index_name,
                '_type': index_type,
                '_id': result.id,
                'id': result.id,
                'source': {
                    'id': result.source.id,
                    'category': result.source.category,
                    'group': result.source.group,
                    'title': result.source.title,
                    'address': result.source.address,
                    'type': result.source.type
                },
                'url': result.final_url,
                'title': result.title,
                'summary': result.summary,
                'image': result.image,
                'published': result.published.astimezone(pytz.UTC)
            })

            min_id = result.id

        helpers.bulk(es, index_data)
        time.sleep(0.3)

    db_session.close()
    db_engine.dispose()


if __name__ == '__main__':
    main()
