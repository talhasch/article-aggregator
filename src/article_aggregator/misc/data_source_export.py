import os

os.sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../../")))

import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from article_aggregator.config import config_get
from article_aggregator.models import DataSource

logging.basicConfig(level=logging.INFO)


def main():
    db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')
    db_engine = create_engine(db_uri)

    Session = sessionmaker(bind=db_engine)
    session = Session()
    data_list = []
    data_sources = session.query(DataSource).order_by(DataSource.id.asc()).all()
    for row in data_sources:
        di = {
            'title': row.title,
            'address': row.address,
            'category': row.category,
            'group': row.group
        }

        data_list.append(di)

    session.close()

    import pprint
    pprint.pprint(data_list)

if __name__ == '__main__':
    main()