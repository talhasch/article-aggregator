from urllib.parse import urlparse


def scrape_rss(address):
    import feedparser
    import requests
    from dateutil.parser import parse as date_parse
    from article_aggregator.util import now_utc
    from datetime import timedelta
    from article_aggregator.constants import RESULT_EXPIRES_IN_DAY
    import pytz

    request_headers = {'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}

    response = requests.get(address, timeout=7, headers=request_headers)
    raw_content = response.content
    d = feedparser.parse(raw_content)

    try:
        title = d.feed.title
    except AttributeError:
        title = None

    try:
        website = d.feed.link
    except AttributeError:
        o = urlparse(address)
        website = '{}://{}'.format(o.scheme, o.netloc)

    data = {'title': title, 'website': website, 'items': []}

    entries = d.entries
    for entry in entries:
        assert 'published' in entry

        published = date_parse(entry['published'])

        if published.tzinfo is None:
            published = published.replace(tzinfo=pytz.utc)

        if published > now_utc():
            continue

        if published < (now_utc() - timedelta(days=RESULT_EXPIRES_IN_DAY)):
            continue

        data['items'].append({
            'title': entry['title'],
            'link': entry['link'],
            'published': published
        })

    return data


def scrape_twitter(address):
    def test_url(url):
        if 'twitter.com' in url:
            return False

        if 'vine.co' in url:
            return False

        if 'youtube.com' in url:
            return False

        return True

    from article_aggregator.db.helper import get_next_twitter_account
    import tweepy
    import pytz
    from article_aggregator.util import now_utc
    from datetime import timedelta
    from article_aggregator.constants import RESULT_EXPIRES_IN_DAY

    t = get_next_twitter_account()

    auth = tweepy.OAuthHandler(t['app_key'], t['app_secret'])
    auth.access_token = t['token']
    auth.access_token_secret = t['token_secret']
    api = tweepy.API(auth)

    user = api.get_user(address)

    title = '{}'.format(user.name)
    try:
        website = user.entities['url']['urls'][0]['expanded_url']
    except KeyError:
        website = None

    data = {'title': title, 'website': website, 'items': []}

    entries = api.user_timeline(screen_name=address, count=100, include_rts=False, tweet_mode='extended')
    for entry in entries:
        published = entry.created_at.replace(tzinfo=pytz.UTC)
        if published > now_utc():
            continue

        if published < (now_utc() - timedelta(days=RESULT_EXPIRES_IN_DAY)):
            continue

        urls = entry.entities['urls']
        for url in urls:
            the_url = url['expanded_url']
            if test_url(the_url):
                data['items'].append({
                    'title': None,
                    'link': the_url,
                    'published': published
                })

    return data


def scrape_facebook(address):
    def test_url(url):
        if 'facebook.com' in url:
            return False

        return True

    from article_aggregator.db.helper import get_next_facebook_account
    from facebook import GraphAPI
    from dateutil.parser import parse as date_parse
    from article_aggregator.util import now_utc
    from datetime import timedelta
    from article_aggregator.constants import RESULT_EXPIRES_IN_DAY

    t = get_next_facebook_account()
    access_token = t['token']

    graph = GraphAPI(access_token)

    user = graph.get_object('/{}?fields=name,username,website'.format(address))
    title = '{}'.format(user['name'])

    try:
        website = user['website']
    except KeyError:
        website = None

    data = {'title': title, 'website': website, 'items': []}

    q = '/{}/posts?fields=link,type,created_time&order=reverse_chronological&limit=100'.format(address)
    posts = graph.get_object(q)['data']
    for entry in posts:
        if entry['type'] != 'link':
            continue

        published = date_parse(entry['created_time'])

        if published > now_utc():
            continue

        if published < (now_utc() - timedelta(days=RESULT_EXPIRES_IN_DAY)):
            continue

        the_url = entry['link']
        if test_url(the_url):
            data['items'].append({
                'title': None,
                'link': the_url,
                'published': published
            })

    return data


def parse_html(url):
    from article_aggregator.article_parser import ArticleParser
    from article_aggregator.constants import MIN_PATH_LEN
    import re

    parser = ArticleParser(url)
    parser.parse()

    title = parser.parsed_title
    summary = parser.summary
    image = parser.parsed_image
    canonical = parser.canonical
    publish_date = parser.publish_date
    is_article = True

    assert title and summary

    # parse url to components
    o = urlparse(canonical)
    scheme = o.scheme
    netloc = o.netloc
    path = o.path

    # check path len
    if len(path) < MIN_PATH_LEN:
        is_article = False

    # if path not contains a numeric value, document must has published date
    if is_article:
        nums_in_path = re.findall(r'\d+', path)
        if len(nums_in_path) == 0:
            if publish_date is None:
                is_article = False

    return {
        'url': {
            'url': canonical,
            'scheme': scheme,
            'netloc': netloc,
            'path': path
        },
        'title': title,
        'summary': summary,
        'image': image,
        'publish_date': publish_date,
        'is_article': is_article
    }
