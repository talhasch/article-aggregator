import logging
import os
import sys
import traceback

os.sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../../")))

from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.models import JobQueue, DataSource, Result, DataSourceError, ResultError
from article_aggregator.util import now_utc
from article_aggregator.tasks.tasks import scrape_rss, scrape_twitter, scrape_facebook, parse_html
from celery import Celery
from celery.bin import worker
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError, IntegrityError

logging.basicConfig(level=logging.INFO)

celery_backend = config_get('CELERY', 'BACKEND')
celery_broker = config_get('CELERY', 'BROKER')
# celery_app = Celery('tasks', backend=celery_backend, broker=celery_broker)
celery_app = Celery('tasks', broker=celery_broker)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')


@celery_app.task
def scrape_task(job_id):
    global db_uri
    db_engine = create_engine(db_uri)

    logging.info('Job id: {}'.format(job_id))

    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()

    job = db_session.query(JobQueue).filter(JobQueue.id == job_id).first()
    if job is None:
        logging.info('Job data not found')
        db_session.close()
        db_engine.dispose()
        return

    data_source_id = job.arg1
    data_source = db_session.query(DataSource).filter(DataSource.id == data_source_id).filter(
        DataSource.status == DATASOURCE_STATUS_ON).first()
    if data_source is None:
        logging.info('Data source data not found')
        db_session.delete(job)
        db_session.commit()
        db_session.close()
        db_engine.dispose()
        return

    fn = None

    if data_source.type == DATASOURCE_TYPE_RSS:
        fn = scrape_rss

    if data_source.type == DATASOURCE_TYPE_TWITTER:
        fn = scrape_twitter

    if data_source.type == DATASOURCE_TYPE_FACEBOOK:
        fn = scrape_facebook

    if fn is None:
        logging.info('Unknown data source type')
        db_session.delete(job)
        db_session.commit()
        db_session.close()
        db_engine.dispose()
        return

    try:
        scraped_data = fn(data_source.address)
        link_list = scraped_data['items']
    except Exception as ex:
        logging.error('Data source scrape error occurred')

        str_tb = ''
        try:
            ex_type, ex, tb = sys.exc_info()
            str_tb = str(traceback.extract_tb(tb))
        except Exception as ex:
            pass

        the_error = DataSourceError()
        the_error.source_id = data_source_id
        the_error.error = str_tb
        db_session.add(the_error)

        link_list = []

    created_count = 0

    for link in link_list:
        the_result = db_session.query(Result).filter(Result.source_id == data_source_id) \
            .filter(Result.url == link['link']) \
            .limit(1).first()

        if the_result is None:
            the_result = Result()
            the_result.source = data_source
            if link['title'] is not None:
                the_result.title = link['title']
            the_result.url = link['link']
            the_result.published = link['published']
            the_result.status = RESULT_STATUS_NEW
            db_session.add(the_result)
            created_count += 1

    data_source.last_run = now_utc()
    db_session.delete(job)
    db_session.commit()
    logging.info('OK - {} new result(s) created'.format(created_count))
    db_session.close()
    db_engine.dispose()
    return True


@celery_app.task
def parse_task(job_id):
    def filters(the_list, value):
        st = False
        for item in the_list:
            if item in value:
                st = True
                break
        return st

    global db_uri
    db_engine = create_engine(db_uri)

    logging.info('Job id: {}'.format(job_id))

    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()

    job = db_session.query(JobQueue).filter(JobQueue.id == job_id).first()
    if job is None:
        logging.info('Job data not found')
        db_session.close()
        db_engine.dispose()
        return

    result_id = job.arg1
    the_result = db_session.query(Result).filter(Result.id == result_id).filter(
        Result.status == RESULT_STATUS_PARSING).first()
    if the_result is None:
        logging.info('Result data not found')
        db_session.delete(job)
        db_session.commit()
        db_session.close()
        db_engine.dispose()
        return

    try:
        parse_result = parse_html(the_result.url)
    except Exception as ex:
        logging.error('Result parse error occurred')

        str_tb = ''
        try:
            ex_type, ex, tb = sys.exc_info()
            str_tb = str(traceback.extract_tb(tb))
        except Exception as ex:
            pass

        the_result.status = RESULT_STATUS_PARSE_FAILED

        the_error = ResultError()
        the_error.result_id = result_id
        the_error.error = str_tb
        db_session.add(the_error)

        the_result.status = RESULT_STATUS_PARSE_FAILED
        db_session.commit()
        return

    final_url = parse_result['url']['url']
    final_url_check = db_session.query(Result).filter(Result.final_url == final_url).first()
    if final_url_check is not None:
        logging.info('SAME FINAL URL FOUND FOR DATASOURCE')
        the_result.final_url_err = final_url
        the_result.status = RESULT_STATUS_ERR_DUPLICATE
    else:
        status = RESULT_STATUS_PARSED

        if not parse_result['is_article']:
            status = RESULT_STATUS_ERR_NOT_ARTICLE

        if the_result.source.filters:
            domain_filters = the_result.source.filters.split(',')
            if not filters(domain_filters, parse_result['url']['netloc']):
                status = RESULT_STATUS_ERR_WRONG_DOMAIN

        the_result.final_url = parse_result['url']['url']
        the_result.scheme = parse_result['url']['scheme']
        the_result.netloc = parse_result['url']['netloc']
        the_result.path = parse_result['url']['path']

        if the_result.title is None:
            the_result.title = parse_result['title']

        the_result.summary = parse_result['summary']
        the_result.image = parse_result['image']

        # update publish date of news if publish_date field is null in database
        # TODO: Date comparison. Select old one.
        if the_result.published is None and parse_result['publish_date']:
            the_result.published = parse_result['publish_date']

        the_result.status = status

    db_session.delete(job)

    try:
        db_session.commit()
    except IntegrityError as ex:
        db_session.rollback()

    db_session.close()
    db_engine.dispose()
    return True


def main():
    w = worker.worker(app=celery_app)
    options = {
        'loglevel': 'ERROR',
        'traceback': True,
    }
    w.run(**options)


if __name__ == '__main__':
    main()
