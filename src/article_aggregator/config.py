
try:
    # PY3
    import configparser as config_parser
except ImportError:
    import ConfigParser as config_parser

import os

config = config_parser.ConfigParser()

config_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.ini')

if not os.path.isfile(config_file_path):
    raise EnvironmentError('Config file config.ini not exists!')

config.read(config_file_path)


def config_get(section, option):
    return config.get(section, option)


def config_get_int(section, option):
    return config.getint(section, option)


def config_get_bool(section, option):
    return config.getboolean(section, option)


if __name__ == '__main__':
    config_val = config_get('SQLALCHEMY', 'DATABASE_URI')
    print(config_val)
