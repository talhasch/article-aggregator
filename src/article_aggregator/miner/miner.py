import logging
import time
from datetime import timedelta

from elasticsearch import Elasticsearch
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError, IntegrityError
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import nullsfirst

from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.index.helper import make_search_body
from article_aggregator.models import Project, Keyword, ProjectResult
from article_aggregator.models import Result
from article_aggregator.util import now_utc

logging.basicConfig(level=logging.INFO)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')

es_host = config_get('ES', 'HOST')
index_name = config_get('ES', 'INDEX_NAME')
index_type = config_get('ES', 'TYPE_NAME')
es = Elasticsearch(es_host)


def miner():
    global db_uri, es

    db_engine = create_engine(db_uri)
    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()

    min_date = now_utc() - timedelta(seconds=60)
    try:
        projects = db_session.query(Project).filter(Project.last_mined < min_date). \
            order_by(nullsfirst(Project.last_mined.asc())).limit(10).all()
    except OperationalError as ex:
        projects = []

    if len(projects) == 0:
        db_session.close()
        db_engine.dispose()
        return

    project_data = {}

    # collect project results
    for project in projects:
        url_cache = []
        project_data[project.id] = []
        keywords = db_session.query(Keyword).filter(Keyword.project_id == project.id).all()
        source_category = project.source_category
        for keyword in keywords:
            search_body = make_search_body(keyword.query, data_source_category=source_category, start_from=0,
                                           page_size=100)
            try:
                search_results = es.search(index=index_name, doc_type=index_type, body=search_body)
                hits = search_results['hits']['hits']
            except Exception as ex:
                hits = []

            for hit in hits:
                row = hit['_source']
                result_id = row['id']
                result_url = row['url']
                result_title = row['title']
                result_summary = row['summary']

                if result_url in url_cache:
                    continue

                result_data = db_session.query(Result).filter(Result.id == result_id).filter(
                    Result.status == RESULT_STATUS_INDEXED).first()
                if result_data is None:
                    continue

                result_source_id = result_data.source_id
                result_source_title = result_data.source.title
                result_published = result_data.published

                project_result_check = db_session.query(ProjectResult).filter(
                    ProjectResult.project_id == project.id).filter(
                    ProjectResult.result_url == result_url).first()
                if project_result_check is not None:
                    continue

                if 'highlight' in hit:
                    if 'title' in hit['highlight']:
                        result_title = hit['highlight']['title'][0]

                    if 'summary' in hit['highlight']:
                        result_summary = hit['highlight']['summary'][0]

                project_data[project.id].append({
                    'result_id': result_id,
                    'result_source_id': result_source_id,
                    'result_source_title': result_source_title,
                    'result_title': result_title,
                    'result_summary': result_summary,
                    'result_published': result_published,
                    'result_url': result_url
                })

                url_cache.append(result_url)

    # insert results
    for project_id, results in project_data.items():
        mined = 0
        for result in results:
            project_result = ProjectResult()
            project_result.project_id = project_id
            project_result.result_id = result['result_id']
            project_result.result_source_id = result['result_source_id']
            project_result.result_source_title = result['result_source_title']
            project_result.result_title = result['result_title']
            project_result.result_summary = result['result_summary']
            project_result.result_published = result['result_published']
            project_result.result_url = result['result_url']

            try:
                db_session.add(project_result)
                db_session.commit()
                mined += 1
            except IntegrityError as ex:
                db_session.rollback()

        if mined > 0:
            logging.info('{} result(s) mined for {} '.format(mined, project_id))

    # update projects
    for project_id, results in project_data.items():
        try:
            project = db_session.query(Project).with_for_update(nowait=True, of=Project). \
                filter(Project.id == project_id).first()
        except OperationalError as ex:
            project = None

        if project is not None:
            project.last_mined = now_utc()

    db_session.commit()

    db_session.close()
    db_engine.dispose()


def main():
    while True:
        miner()
        time.sleep(1)


if __name__ == '__main__':
    main()
