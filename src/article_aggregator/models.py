from datetime import datetime, timedelta

import pytz
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, SmallInteger, BigInteger, \
    Text, Index, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import backref

from .constants import *
from .util import now_utc

__all__ = ['Base', 'DataSource', 'DataSourceError', 'Result',
           'ResultError', 'JobQueue', 'App', 'Account', 'AutoPostAccount']

Base = declarative_base()


def default_last_triggered():
    return datetime.utcnow().replace(tzinfo=pytz.utc) - timedelta(minutes=5)


def default_account_last_used():
    return datetime.utcnow().replace(tzinfo=pytz.utc) - timedelta(minutes=1)


def default_project_last_mined():
    return datetime.utcnow().replace(tzinfo=pytz.utc) - timedelta(minutes=5)


class DataSource(Base):
    __tablename__ = 'data_sources'

    id = Column('id', Integer, primary_key=True)
    type = Column('type', SmallInteger, nullable=False)  # 1=RSS, 2=Twitter, 3=Facebook
    category = Column('category', String, nullable=False)
    group = Column('group', String, nullable=False)
    title = Column('title', String, nullable=False)
    address = Column('address', String, nullable=False)
    filters = Column('filters', String)
    last_triggered = Column('last_triggered', DateTime(timezone=True), nullable=False, default=default_last_triggered)
    last_run = Column('last_run', DateTime(timezone=True))
    status = Column('status', SmallInteger, nullable=False, default=DATASOURCE_STATUS_ON)  # 1=On, 2=Off
    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)
    modified = Column('modified', DateTime(timezone=True), nullable=False, default=now_utc)

    def __str__(self):
        return self.title

    __mapper_args__ = {
        "order_by": id.desc()
    }


UniqueConstraint(DataSource.type, DataSource.address, name='uix_data_sources_type_address')


class DataSourceError(Base):
    __tablename__ = 'data_source_errors'

    id = Column('id', Integer, primary_key=True)
    source_id = Column('source', BigInteger, ForeignKey(DataSource.id, ondelete='CASCADE'))
    source = relationship(DataSource, backref=backref('DataSourceError', cascade="all, delete"))
    error = Column('error', Text)
    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)


Index('ix_data_source_errors_source', DataSourceError.source_id)


class Result(Base):
    __tablename__ = 'results'

    id = Column('id', BigInteger, primary_key=True)
    source_id = Column('source', Integer, ForeignKey(DataSource.id, ondelete='SET NULL'))
    source = relationship(DataSource)
    url = Column('url', String, nullable=False)
    final_url = Column('final_url', String)
    final_url_err = Column('final_url_err', String)
    scheme = Column('scheme', String)
    netloc = Column('netloc', String)
    path = Column('path', String)
    title = Column('title', String)
    summary = Column('summary', Text)
    image = Column('image', String)
    published = Column('published', DateTime(timezone=True))
    status = Column('status', SmallInteger, nullable=False,
                    default=1)  # see RESULT_STATUS_* in constants.py
    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)

    __mapper_args__ = {
        "order_by": id.desc()
    }


UniqueConstraint(Result.final_url, name='uix_results_final_url')
Index('ix_results_source', Result.source_id)
Index('ix_results_status', Result.status)
Index('ix_results_created_asc', Result.created.asc())
Index('ix_results_published_desc', Result.published.desc())
Index('ix_results_published_asc', Result.published.asc())


class ResultError(Base):
    __tablename__ = 'result_errors'

    id = Column('id', Integer, primary_key=True)
    result_id = Column('result', BigInteger, ForeignKey(Result.id, ondelete='CASCADE'))
    source = relationship(Result, backref=backref('ResultError', cascade="all, delete"))
    error = Column('error', Text)
    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)


Index('ix_result_errors_result', ResultError.result_id)


class JobQueue(Base):
    __tablename__ = 'job_queue'

    id = Column('id', BigInteger, primary_key=True)
    type = Column('type', Integer, nullable=False)
    arg1 = Column('arg1', String, nullable=False)
    arg2 = Column('arg2', String)
    arg3 = Column('arg3', String)
    arg4 = Column('arg4', String)
    arg5 = Column('arg5', String)
    status = Column('status', SmallInteger, default=JOB_QUEUE_STATUS_ON, nullable=False)
    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)
    execute_date = Column('execute_date', DateTime(timezone=True), nullable=False)
    executed_date = Column('executed_date', DateTime(timezone=True))
    try_count = Column('try_count', SmallInteger, default=0)


Index('ix_job_queue_type', JobQueue.type)
Index('ix_job_queue_status', JobQueue.status)
Index('ix_job_queue_created_asc', JobQueue.created.asc())
Index('ix_job_execute_date_asc', JobQueue.execute_date.asc())


class App(Base):
    __tablename__ = 'app'

    id = Column('id', Integer, primary_key=True)
    type = Column('type', SmallInteger, nullable=False)  # 1=Twitter, 2=Facebook
    name = Column('name', String, nullable=False)
    key = Column('key', String, nullable=False)
    secret = Column('secret', String, nullable=False)
    status = Column('status', SmallInteger, nullable=False, default=0)  # 1=On, 2=Off
    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)

    def __str__(self):
        return self.name

    __mapper_args__ = {
        "order_by": id.desc()
    }


class Account(Base):
    __tablename__ = 'account'

    id = Column('id', Integer, primary_key=True)
    app_id = Column('app', Integer, ForeignKey(App.id, ondelete='RESTRICT'), nullable=False)
    app = relationship(App)
    name = Column('name', String, nullable=False, unique=True)
    token = Column('token', String, nullable=False)
    token_secret = Column('token_secret', String)
    status = Column('status', SmallInteger, nullable=False, default=0)  # 1=On, 2=Off
    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)
    last_used = Column('last_used', DateTime(timezone=True), nullable=False, default=default_account_last_used)

    __mapper_args__ = {
        "order_by": id.desc()
    }


class Project(Base):
    __tablename__ = 'projects'

    id = Column('id', Integer, primary_key=True)
    title = Column('title', String, nullable=False)
    slug = Column('slug', String, nullable=False, unique=True)
    source_category = Column('source_category', String)
    last_mined = Column('last_mined', DateTime(timezone=True), nullable=False, default=default_project_last_mined)
    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)

    __mapper_args__ = {
        "order_by": id.desc()
    }

    def __str__(self):
        return self.title


class Keyword(Base):
    __tablename__ = 'keywords'

    id = Column('id', Integer, primary_key=True)
    project_id = Column('project', Integer, ForeignKey(Project.id, ondelete='CASCADE'), nullable=False)
    project = relationship(Project, backref=backref('Keyword', cascade="all, delete"))
    query = Column('query', String, nullable=False)


Index('ix_keywords_project', Keyword.project_id)


class ProjectResult(Base):
    __tablename__ = 'project_results'

    id = Column('id', BigInteger, primary_key=True)

    project_id = Column('project', Integer, ForeignKey(Project.id, ondelete='CASCADE'), nullable=False)
    project = relationship(Project, backref=backref('ProjectResult', cascade="all, delete"))

    result_id = Column('result', Integer, ForeignKey(Result.id, ondelete='CASCADE'), nullable=False)
    result = relationship(Result, backref=backref('ProjectResult', cascade="all, delete"))

    result_source_id = Column('result_source_id', Integer, nullable=False)
    result_source_title = Column('result_source_title', String, nullable=False)

    result_title = Column('result_title', String)
    result_summary = Column('result_summary', String)
    result_published = Column('result_published', DateTime(timezone=True))
    result_url = Column('url', String, nullable=False)

    blocked = Column('blocked', SmallInteger, nullable=False, default=0)

    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)

    # a helper flag column
    flag1 = Column('flag1', SmallInteger, nullable=False, default=1)


Index('ix_project_results_project', ProjectResult.project_id)
Index('ix_project_results_result', ProjectResult.result_id)
Index('ix_project_results_result_source_id', ProjectResult.result_source_id)
Index('ix_project_results_result_source_title', ProjectResult.result_source_title)
Index('ix_project_results_result_published', ProjectResult.result_published)
Index('ix_project_results_result_published_desc', ProjectResult.result_published.desc())
Index('ix_project_results_blocked', ProjectResult.blocked)
Index('ix_project_results_flag1', ProjectResult.flag1)
UniqueConstraint(ProjectResult.project_id, ProjectResult.result_url, name='uix_project_results_project_result_url')


class AutoPostAccount(Base):
    __tablename__ = 'auto_post_accounts'

    id = Column('id', BigInteger, primary_key=True)
    project_id = Column('project', Integer, ForeignKey(Project.id, ondelete='CASCADE'), nullable=False)
    project = relationship(Project, backref=backref('AutoPostAccount', cascade="all, delete"))
    type = Column('type', SmallInteger, nullable=False)
    name = Column('name', String, nullable=False)
    username = Column('username', String, nullable=False)
    key = Column('key', String, nullable=False)
    secret = Column('secret', String, nullable=False)
    key2 = Column('key2', String)
    secret2 = Column('secret2', String)
    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)
    status = Column('status', SmallInteger, nullable=False, default=1)
    tags = Column('tags', String(80))

    __mapper_args__ = {
        "order_by": id.desc()
    }


UniqueConstraint(AutoPostAccount.project_id, AutoPostAccount.type, AutoPostAccount.username,
                 name='uix_auto_post_accounts_project_id_type_username')


class AutoPostLog(Base):
    __tablename__ = 'auto_post_logs'

    id = Column('id', BigInteger, primary_key=True)

    project_id = Column('project', Integer, ForeignKey(Project.id, ondelete='CASCADE'), nullable=False)
    project = relationship(Project, backref=backref('AutoPostLog', cascade="all, delete"))

    result_id = Column('result', BigInteger, ForeignKey(Result.id, ondelete='CASCADE'))
    result = relationship(Result, backref=backref('AutoPostLog', cascade="all, delete"))

    account_id = Column('account', BigInteger, ForeignKey(AutoPostAccount.id, ondelete='CASCADE'))
    account = relationship(AutoPostAccount, backref=backref('AutoPostLog', cascade="all, delete"))

    has_error = Column('has_error', SmallInteger, nullable=False, default=0)

    message = Column('message', String)

    created = Column('created', DateTime(timezone=True), nullable=False, default=now_utc)
