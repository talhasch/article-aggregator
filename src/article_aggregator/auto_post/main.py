import io
import logging
import os
import tempfile
import time
from random import randint

import requests
import tweepy
from PIL import Image
from elasticsearch import Elasticsearch
from facebook import GraphAPI
from slugify import slugify
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.models import ProjectResult, AutoPostAccount, AutoPostLog

logging.basicConfig(level=logging.INFO)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')

es_host = config_get('ES', 'HOST')
index_name = config_get('ES', 'INDEX_NAME')
index_type = config_get('ES', 'TYPE_NAME')
es = Elasticsearch(es_host)


def copy_image(image_url):
    # get image data
    image_data = requests.get(image_url).content

    # convert data to image
    im = Image.open(io.BytesIO(image_data))

    # create random file in system's temp directory
    image_path = os.path.join(tempfile.gettempdir(), '{}.jpg'.format(str(randint(0, 10000000))))

    # save image to file
    im.save(image_path, quality=100)

    return image_path


def prepare_link(result, project):
    link_base = config_get('AUTO_POST', 'LINK_BASE')
    slug = slugify(result.title, to_lower=True)
    return '{}/{}/{}-{}'.format(link_base, project.slug, slug, result.id)


def prepare_tweet_text(result, project, tags=None):
    link = prepare_link(result, project)

    title = result.title
    trunc_text = (title[:85] + '..') if len(title) > 85 else title

    source_title = result.source.title[:28]

    str_tags = ''
    if tags is not None and tags.strip():
        str_tags = ' '.join(list(map(lambda x: x if x.startswith('#') else '#' + x.strip(), tags.split(','))))

    return "{} {} (Source: {}) {}".format(trunc_text, link, source_title, str_tags).strip()


def prepare_facebook_text(result, tags=None):
    title = result.title
    source_title = result.source.title

    str_tags = ''
    if tags is not None and tags.strip():
        str_tags = ' '.join(list(map(lambda x: x if x.startswith('#') else '#' + x.strip(), tags.split(','))))

    return "{}\n(Source: {}) {}".format(title, source_title, str_tags).strip()


def worker():
    global db_uri, es

    db_engine = create_engine(db_uri)
    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()

    project_result = db_session.query(ProjectResult).with_for_update(nowait=True, of=ProjectResult).filter(
        ProjectResult.flag1 == 1).order_by(
        ProjectResult.id.asc()).first()

    if project_result is None:
        db_session.close()
        db_engine.dispose()
        return

    project_id = project_result.project_id

    # Auto post accounts for the project_id
    auto_post_accounts = db_session.query(AutoPostAccount).filter(AutoPostAccount.project_id == project_id).filter(
        AutoPostAccount.status == AUTO_POST_ACCOUNT_STATUS_ON).all()

    for auto_post_account in auto_post_accounts:
        result = project_result.result

        # Twitter Status
        if auto_post_account.type == AUTO_POST_ACCOUNT_TYPE_TWITTER:
            result_id = result.id
            image = result.image

            with_image = False
            image_path = None
            image_path = ''
            """
            if image:
                try:
                    image_path = copy_image(image)
                    with_image = True
                except Exception as ex:
                    pass
            """

            tweet_body = prepare_tweet_text(result, project_result.project, tags=auto_post_account.tags)

            api_key = auto_post_account.key
            api_secret = auto_post_account.secret
            access_token = auto_post_account.key2
            access_token_secret = auto_post_account.secret2

            auth = tweepy.OAuthHandler(api_key, api_secret)
            auth.access_token = access_token
            auth.access_token_secret = access_token_secret
            api = tweepy.API(auth)

            if with_image:
                fn = api.update_with_media
                args = {'filename': image_path, 'status': tweet_body}
            else:
                fn = api.update_status
                args = {'status': tweet_body}

            has_error = False
            error_msg = None
            try:
                fn(**args)
            except Exception as ex:
                has_error = True
                error_msg = str(ex)

            if with_image and os.path.exists(image_path):
                os.remove(image_path)

            if has_error:
                log = AutoPostLog()
                log.project_id = project_id
                log.result_id = result_id
                log.account_id = auto_post_account.id
                log.has_error = 1
                log.message = error_msg
                db_session.add(log)
            else:
                log = AutoPostLog()
                log.project_id = project_id
                log.result_id = result_id
                log.account_id = auto_post_account.id
                db_session.add(log)

        # Facebook page post
        if auto_post_account.type == AUTO_POST_ACCOUNT_TYPE_FACEBOOK:
            result_id = result.id

            post_text = prepare_facebook_text(result, tags=auto_post_account.tags)
            link = prepare_link(result, project_result.project)
            graph = GraphAPI(auto_post_account.key2)

            has_error = False
            error_msg = None
            try:
                graph.put_object('/{}'.format(auto_post_account.username), 'feed', message=post_text, link=link)
            except Exception as ex:
                has_error = True
                error_msg = str(ex)

            if has_error:
                log = AutoPostLog()
                log.project_id = project_id
                log.result_id = result_id
                log.account_id = auto_post_account.id
                log.has_error = 1
                log.message = error_msg
                db_session.add(log)
            else:
                log = AutoPostLog()
                log.project_id = project_id
                log.result_id = result_id
                log.account_id = auto_post_account.id
                db_session.add(log)

    project_result.flag1 = 0
    db_session.commit()

    db_session.close()
    db_engine.dispose()


def main():
    while True:
        worker()
        time.sleep(1)


if __name__ == '__main__':
    main()
