import logging
import time
from datetime import timedelta

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker

from article_aggregator.config import config_get
from article_aggregator.constants import *
from article_aggregator.models import Result
from article_aggregator.util import now_utc

logging.basicConfig(level=logging.INFO)

db_uri = config_get('SQLALCHEMY', 'DATABASE_URI')

es_host = config_get('ES', 'HOST')
index_name = config_get('ES', 'INDEX_NAME')
index_type = config_get('ES', 'TYPE_NAME')
es = Elasticsearch(es_host)


def cleaner():
    global db_uri, es
    db_engine = create_engine(db_uri)
    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()

    results = db_session.query(Result).filter(Result.status == RESULT_STATUS_DELETED) \
        .order_by(Result.id.asc()).limit(100).all()

    if len(results) == 0:
        db_session.close()
        db_engine.dispose()
        return

    deleted = 0
    for result in results:
        try:
            es.delete(index=index_name, doc_type=index_type, id=result.id)
        except NotFoundError as ex:
            pass

        db_session.delete(result)
        deleted += 1

    db_session.commit()
    db_session.close()
    db_engine.dispose()

    logging.info('{} result(s) deleted'.format(deleted))


def marker():
    global db_uri, es
    db_engine = create_engine(db_uri)
    db_session_maker = sessionmaker(bind=db_engine)
    db_session = db_session_maker()

    min_date_published = now_utc() - timedelta(days=30)
    try:
        results = db_session.query(Result).with_for_update(nowait=True, of=Result). \
            filter(Result.status != RESULT_STATUS_DELETED). \
            filter(Result.published < min_date_published). \
            filter(Result.published.isnot(None)). \
            order_by(Result.published.asc()).limit(100).all()
    except OperationalError as ex:
        results = []

    if len(results) == 0:
        db_session.close()
        db_engine.dispose()
        return

    marked = 0
    for result in results:
        result.status = RESULT_STATUS_DELETED
        marked += 1

    db_session.commit()
    db_session.close()
    db_engine.dispose()

    logging.info('{} result(s) marked for delete'.format(marked))


def main():
    while True:
        marker()
        cleaner()
        time.sleep(4)


if __name__ == '__main__':
    main()
