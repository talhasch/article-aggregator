import os

os.sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../", "../")))

from article_aggregator.tasks.tasks import scrape_twitter

accounts = [
    'officialpvfc'
]

for account in accounts:
    print(account)
    data = scrape_twitter(account)
    print(data['title'])
    print('--------------------------------------------')
    for row in data['items']:
        print(row)
