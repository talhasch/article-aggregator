from ..tasks.tasks import scrape_rss


feeds = [
    'http://www.getwestlondon.co.uk/all-about/brentford-fc?service=rss',
    'http://www.bbc.co.uk/sport/football/teams/brentford/rss.xml',
    'http://www.90min.com/teams/brentford.rss',
    'http://www.seagulls.co.uk/common/rss/news-rss-feed.xml',
    'http://www.theargus.co.uk/sport/albion/rss/',
    'http://www.90min.com/teams/brighton.rss',
    'http://www.bbc.co.uk/sport/football/teams/brighton-and-hove-albion/rss.xml',
    'http://www.bcfc.com/common/rss/news-rss-feed.xml',
    'http://www.birminghammail.co.uk/all-about/birmingham-city-fc?service=rss',
    'http://www.bbc.co.uk/sport/football/teams/birmingham-city/rss.xml',
    'http://www.bcfc.co.uk/common/rss/news-rss-feed.xml',
    'http://www.bbc.co.uk/sport/football/teams/bristol-city/rss.xml',
    'http://www.burtonalbionfc.co.uk/common/rss/news-rss-feed.xml',
    'http://www.burtonmail.co.uk/burton-albion-fc.rss',
    'http://www.bbc.co.uk/sport/football/teams/burton-albion/rss.xml',
    'http://www.cardiffcityfc.co.uk/common/rss/news-rss-feed.xml',
    'http://www.walesonline.co.uk/all-about/cardiff-city-fc?service=rss',
    'http://www.bbc.co.uk/sport/football/teams/cardiff-city/rss.xml',
    'http://www.dcfc.co.uk/common/rss/news-rss-feed.xml',
    'http://www.derbytelegraph.co.uk/derbycounty.rss',
    'http://www.bbc.co.uk/sport/football/teams/derby-county/rss.xml',
    'http://www.football.co.uk/teams/fulham/rss.xml',
    'http://www.fulhamweb.co.uk/rss.aspx',
    'http://www.bbc.co.uk/sport/football/teams/fulham/rss.xml',
    'http://www.htafc.com/common/rss/news-rss-feed.xml',
    'http://www.football.co.uk/teams/huddersfield-town/rss.xml',
    'http://www.bbc.co.uk/sport/football/teams/huddersfield-town/rss.xml',
    'http://www.itfc.co.uk/common/rss/news-rss-feed.xml',
    'http://www.twtd.co.uk/rss/',
    'http://www.bbc.co.uk/sport/football/teams/ipswich-town/rss.xml',
    'http://www.football.co.uk/teams/leeds-united/rss.xml',
    'http://www.bbc.co.uk/sport/football/teams/leeds-united/rss.xml',
    'http://www.nottinghamforest.co.uk/common/rss/news-rss-feed.xml',
    'http://www.nottinghampost.com/nottinghamforest.rss',
    'http://www.bbc.co.uk/sport/football/teams/nottingham-forest/rss.xml',
    'http://www.pnefc.net/common/rss/news-rss-feed.xml',
    'http://www.bbc.co.uk/sport/football/teams/preston-north-end/rss.xml',
    'http://www.qpr.co.uk/common/rss/news-rss-feed.xml',
    'http://www.football.co.uk/teams/queens-park-rangers/rss.xml',
    'http://www.bbc.co.uk/sport/football/teams/queens-park-rangers/rss.xml',
    'http://www.readingfc.co.uk/common/rss/news-rss-feed.xml',
    'http://www.football.co.uk/teams/reading/rss.xml',
    'http://www.bbc.co.uk/sport/football/teams/reading/rss.xml',
    'http://www.themillers.co.uk/common/rss/news-rss-feed.xml',
    'http://www.rotherhamadvertiser.co.uk/standardrss.ashx?c=34',
    'http://www.chroniclelive.co.uk/all-about/newcastle-united-fc?service=rss',
    'http://www.dailymail.co.uk/sport/teampages/newcastle-united.rss',
    'http://www.canaries.co.uk/common/rss/news-rss-feed.xml',
    'http://www.edp24.co.uk/cmlink/edp24_sport_norwich_city_1_595902',
    'http://www.dailymail.co.uk/sport/teampages/norwich-city.rss',
    'http://www.swfc.co.uk/common/rss/news-rss-feed.xml'
]

for feed_url in feeds:
    print(feed_url)
    data = scrape_rss(feed_url)
    print(data['title'])
    print('--------------------------------------------')