import os
import tweepy
os.sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../../")))

from article_aggregator.db.helper import get_next_twitter_account
import requests
t = get_next_twitter_account()

import pprint


auth = tweepy.OAuthHandler(t['app_key'], t['app_secret'])
auth.access_token = t['token']
auth.access_token_secret = t['token_secret']
api = tweepy.API(auth)


def test_url(url):
    if 'twitter.com' in url:
        return False

    if 'vine.co' in url:
        return False

    return True

timeline = api.user_timeline(screen_name='manutd', count=100)
for item in timeline:
    published = item.created_at
    urls = item.entities['urls']
    for url in urls:
        the_url = url['expanded_url']
        if test_url(the_url):
            print(published)
            print(the_url)
            resp = requests.get(the_url)
            print(resp.url)

            print("----------------")
