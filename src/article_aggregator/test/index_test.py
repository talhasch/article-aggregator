
import os
os.sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../../")))

from article_aggregator.index.helper import make_search_body
from elasticsearch import Elasticsearch
from article_aggregator.config import config_get

es_host = config_get('ES', 'HOST')
index_name = config_get('ES', 'INDEX_NAME')
index_type = config_get('ES', 'TYPE_NAME')
es = Elasticsearch(es_host)


search_body = make_search_body("*", data_source_category='arsenal')


search_results = es.search(index=index_name, doc_type=index_type, body=search_body)

total = search_results['hits']['total']

print("Total :%s" % total)
