import argparse
import os

os.sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../")))


def main():
    parser = argparse.ArgumentParser(description='')
    cmd_list = (
        'ui',

        'create_db',
        'drop_db',
        'seed_db',

        'create_index',
        'drop_index',
        'build_index',

        'job_queue_create',
        'job_queue_run',
        'job_queue_recover',

        'tasks',

        'indexer',

        'miner',

        'clean_results',

        'auto_post'
    )

    parser.add_argument('cmd', choices=cmd_list)

    args = parser.parse_args()
    cmd = args.cmd

    if cmd == 'ui':
        from article_aggregator.ui.app import main
        main()

    if cmd == 'create_db':
        from article_aggregator.db.create import main
        main()

    if cmd == 'drop_db':
        from article_aggregator.db.drop import main
        main()

    if cmd == 'seed_db':
        from article_aggregator.db.seed import main
        main()

    if cmd == 'create_index':
        from article_aggregator.index.create import main
        main()

    if cmd == 'drop_index':
        from article_aggregator.index.drop import main
        main()

    if cmd == 'build_index':
        from article_aggregator.index.build import main
        main()

    if cmd == 'job_queue_create':
        from article_aggregator.job_queue.create import main
        main()

    if cmd == 'job_queue_run':
        from article_aggregator.job_queue.run import main
        main()

    if cmd == 'job_queue_recover':
        from article_aggregator.job_queue.recover import main
        main()

    if cmd == 'tasks':
        from article_aggregator.tasks.run import main
        main()

    if cmd == 'indexer':
        from article_aggregator.index.indexer import main
        main()

    if cmd == 'miner':
        from article_aggregator.miner.miner import main
        main()

    if cmd == 'clean_results':
        from article_aggregator.cleaner.clean_results import main
        main()

    if cmd == 'auto_post':
        from article_aggregator.auto_post.main import main
        main()


if __name__ == '__main__':
    main()
