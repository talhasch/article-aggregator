### BASIC CONFIG

#### Change ssh port
$ sudo nano /etc/ssh/sshd_config
set=Port 5543

$ sudo reboot

#### Update && Upgrade
$ sudo apt-get update
$ sudo apt-get upgrade

### BASIC PACKAGES
$ sudo apt-get install htop

### PYTHON
$ sudo apt install virtualenv
$ sudo apt-get install python-dev
$ sudo apt-get install python3-dev
$ sudo apt-get build-dep python-psycopg2
$ sudo apt-get install libpq-dev
$ sudo apt-get install libxml2-dev libxslt1-dev


### INSTALL APP
#### Clone Repo
$ cd ~
$ git clone https://bitbucket.org/talhasch/article-aggregator
$ cd article-aggregator

#### Create virtual environment
$ virtualenv -p python3 venv
$ source venv/bin/activate

#### Install python packages
pip install -r requirements.txt

### SUPERVISOR

$ sudo apt-get install supervisor
$ sudo systemctl enable supervisor

$ sudo nano /etc/supervisor/conf.d/tasks.conf
 
[program:tasks]
command=/home/ubuntu/article-aggregator/venv/bin/python run.py tasks
directory=/home/ubuntu/article-aggregator/src/article_aggregator/
autostart=true
autorestart=true
startsecs=10
stopwaitsecs=600
user=ubuntu
