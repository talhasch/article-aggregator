### BASIC CONFIG

#### Change ssh port
$ sudo nano /etc/ssh/sshd_config
set=Port 5543

$ sudo reboot

#### Update && Upgrade
$ sudo apt-get update
$ sudo apt-get upgrade

### BASIC PACKAGES
$ sudo apt-get install htop

### RABBITMQ
$ sudo apt-get install rabbitmq-server

#### Add a user to rabbitmq
$ sudo rabbitmqctl add_user my_rmq_user us3r-very-ver1l0ng.passwd

#### Set user permissions
$ sudo rabbitmqctl set_permissions -p / my_rmq_user ".*" ".*" ".*"

### REDIS
$ sudo apt-get install redis-server

#### Change bind ip adress of redis
$ sudo nano /etc/redis/redis.conf
set=bind 0.0.0.0
 
$ sudo service redis-server restart

