### BASIC CONFIG

#### Change ssh port
$ sudo nano /etc/ssh/sshd_config
set=Port 5543

$ sudo reboot

#### Update && Upgrade
$ sudo apt-get update
$ sudo apt-get upgrade

### BASIC PACKAGES
$ sudo apt-get install htop

### PYTHON
$ sudo apt install virtualenv
$ sudo apt-get install python-dev
$ sudo apt-get install python3-dev
$ sudo apt-get build-dep python-psycopg2
$ sudo apt-get install libpq-dev
$ sudo apt-get install libxml2-dev libxslt1-dev

### POSTGRESQL
$ sudo apt-get install postgresql postgresql-contrib pgadmin3

## Postgresql config: http://pgtune.leopard.in.ua
$ sudo nano /etc/postgresql/9.5/main/postgresql.conf
$ sudo service postgresql restart

## Change postgres user pass
$ sudo -u postgres psql
> ALTER USER postgres PASSWORD 'j4u+XrmPc0';

## Create database
$ sudo -u postgres psql
> create database article_aggregator;

## Allow remote access
$ sudo nano /etc/postgresql/9.5/main/postgresql.conf
modify=listen_addresses='*'
$ sudo service postgresql restart

$ sudo nano /etc/postgresql/9.5/main/pg_hba.conf
add=host	all	all	0.0.0.0/0	md5
$ sudo service postgresql restart

### ELASTICSEARCH

#### Instal Java 8
$ sudo add-apt-repository -y ppa:webupd8team/java
$ sudo apt-get update
$ sudo apt-get -y install oracle-java8-installer

#### Install Elasticsearch
$ wget https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/2.3.1/elasticsearch-2.3.1.deb
$ sudo dpkg -i elasticsearch-2.3.1.deb
$ sudo systemctl enable elasticsearch.service

#### Elasticsearch Config
$ sudo nano /etc/elasticsearch/elasticsearch.yml

set=network.host: 0.0.0.0
set=bootstrap.mlockall: true

$ sudo nano /etc/init.d/elasticsearch
set=ES_HEAP_SIZE=2g


### INSTALL APP
#### Clone Repo
$ cd ~
$ git clone https://bitbucket.org/talhasch/article-aggregator
$ cd article-aggregator

#### Create virtual environment
$ virtualenv -p python3 venv
$ source venv/bin/activate

#### Install python packages
pip install -r requirements.txt

### SERVICES
#### Job Queue Create
$ sudo cp script/init/job-queue-create /etc/init.d/job-queue-create
$ sudo chmod +x /etc/init.d/job-queue-create
$ sudo update-rc.d job-queue-create defaults
$ sudo service job-queue-create start

#### Job Queue Run
$ sudo cp script/init/job-queue-run /etc/init.d/job-queue-run
$ sudo chmod +x /etc/init.d/job-queue-run
$ sudo update-rc.d job-queue-run defaults
$ sudo service job-queue-run start

#### Job Queue Recover
$ sudo cp script/init/job-queue-recover /etc/init.d/job-queue-recover
$ sudo chmod +x /etc/init.d/job-queue-recover
$ sudo update-rc.d job-queue-recover defaults
$ sudo service job-queue-recover start

#### Indexer
$ sudo cp script/init/indexer /etc/init.d/indexer
$ sudo chmod +x /etc/init.d/indexer
$ sudo update-rc.d indexer defaults
$ sudo service indexer start

#### Miner
$ sudo cp script/init/miner /etc/init.d/miner
$ sudo chmod +x /etc/init.d/miner
$ sudo update-rc.d miner defaults
$ sudo service miner start

#### Result Cleaner
$ sudo cp script/init/clean-results /etc/init.d/clean-results
$ sudo chmod +x /etc/init.d/clean-results
$ sudo update-rc.d clean-results defaults
$ sudo service clean-results start

#### Web UI
$ sudo cp script/init/web-ui /etc/init.d/web-ui
$ sudo chmod +x /etc/init.d/web-ui
$ sudo update-rc.d web-ui defaults
$ sudo service web-ui start

#### Auto Post
$ sudo cp script/init/auto-post /etc/init.d/auto-post
$ sudo chmod +x /etc/init.d/auto-post
$ sudo update-rc.d auto-post defaults
$ sudo service auto-post start

##### Reserve proxy with nginx

$ sudo apt-get install nginx
$ sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bck
$ sudo cp doc/nginx /etc/nginx/sites-available/default
$ sudo service nginx restart

##### Static file permissions
$ sudo chmod 777 -R 'src/article_aggregator/ui/static/.webassets-cache'
$ sudo chmod 777 -R 'src/article_aggregator/ui/static/gen'


###### nginx basic auth
https://www.digitalocean.com/community/tutorials/how-to-set-up-http-authentication-with-nginx-on-ubuntu-12-10

$ sudo apt-get install apache2-utils

$ sudo htpasswd -c /etc/nginx/.htpasswd aag
d8orpnmdkfh

### MONIT
#### Install

$ sudo apt-get install monit
$ sudo apt-get install postfix

#### Config

$ sudo nano /etc/monit/conf.d/common
"""
set daemon 20

set httpd port 2812 and
use address localhost
allow localhost

set mailserver localhost
set mail-format { from: talhabugrabulut@gmail.com }
set alert talhabugrabulut@gmail.com
"""

$ sudo nano /etc/monit/conf-available/aa
"""
check process web-ui with pidfile /var/run/web-ui.pid
    start program = "/etc/init.d/web-ui start" with timeout 20 seconds
    stop program  = "/etc/init.d/web-ui stop"

check process job-queue-create with pidfile /var/run/job-queue-create.pid
    start program = "/etc/init.d/job-queue-create start" with timeout 20 seconds
    stop program  = "/etc/init.d/job-queue-create stop"

check process job-queue-run with pidfile /var/run/job-queue-run.pid
    start program = "/etc/init.d/job-queue-run start" with timeout 20 seconds
    stop program  = "/etc/init.d/job-queue-run stop"

check process job-queue-recover with pidfile /var/run/job-queue-recover.pid
    start program = "/etc/init.d/job-queue-recover start" with timeout 20 seconds
    stop program  = "/etc/init.d/job-queue-recover stop"

check process indexer with pidfile /var/run/indexer.pid
    start program = "/etc/init.d/indexer start" with timeout 20 seconds
    stop program  = "/etc/init.d/indexer stop"
    
check process miner with pidfile /var/run/miner.pid
    start program = "/etc/init.d/miner start" with timeout 20 seconds
    stop program  = "/etc/init.d/miner stop"

check process clean-results with pidfile /var/run/clean-results.pid
    start program = "/etc/init.d/clean-results start" with timeout 20 seconds
    stop program  = "/etc/init.d/clean-results stop"
    
check process auto-post with pidfile /var/run/auto-post.pid
    start program = "/etc/init.d/auto-post start" with timeout 20 seconds
    stop program  = "/etc/init.d/auto-post stop"

check process elasticsearch with pidfile /var/run/elasticsearch/elasticsearch.pid
    start program = "/etc/init.d/elasticsearch start" with timeout 20 seconds
    stop program  = "/etc/init.d/elasticsearch stop"
"""

$ sudo ln -s /etc/monit/conf-available/aa /etc/monit/conf-enabled/aa

$ sudo service monit restart

### SUPERVISOR

$ sudo apt-get install supervisor
$ sudo systemctl enable supervisor

$ sudo nano /etc/supervisor/conf.d/web.conf
 
[program:web]
command=/home/ubuntu/article-aggregator/script/web-ui
directory=/home/ubuntu/article-aggregator/
autostart=true
autorestart=true
startsecs=10
stopwaitsecs=600
user=root